using System;

class Test
{
	static void Main(){

		//assigning var x 
		int x = 12*30;

		//output x to console
		Console.WriteLine("X = " + x);
		
		//throwing var x to feettoinches func
		Console.WriteLine(FeetToInches (x));

	}

	// a function
	static int FeetToInches (int feet){
		int inches = feet*12;
		return inches;
	}
}	