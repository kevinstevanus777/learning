-- user lagi di hr 
SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                       3300

SQL> insert into karyawan values('123','test','100');

1 row created.

SQL> commit;

Commit complete.

SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                       3300
123 test                        100


SQL >


-- lagi di user sys
--mau liat kondisi flashbacknya

--CARA NYALAIN FLASHBACK
--shutdown immediate
--startup mount
--alter database archive log(wajib dinyalain archive log nya)
--alter database flashback on;
--alter database open;
--select flashback_on from v$database;

--status flashback harus dalam keadaan on bila ingin melakukan truncate table




--lagi di sys
SQL> select current_scn from v$database;

CURRENT_SCN
-----------
   14048948


--di user test
SQL> truncate table karyawan;

Table truncated.

SQL> select * from karyawan;

no rows selected


--ini di sys

SQL> --flashback
SQL> shut immediate;
Database closed.
Database dismounted.
ORACLE instance shut down.
SQL> startup mount;
ORACLE instance started.

Total System Global Area  456146944 bytes
Fixed Size                  1344840 bytes
Variable Size             352324280 bytes
Database Buffers           96468992 bytes
Redo Buffers                6008832 bytes
Database mounted.
SQL> FLASHBACK DATABASE TO SCN 14048948;

Flashback complete.

SQL> alter database open resetlogs;


--di user test
SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                       3300
123 test                        100



-----------------------------------------------------------------------------------------------------
SQL> create user another identified by oracle; 

User created.

SQL> grant all privileges to another;

Grant succeeded.
