--MINGGU 1 SETELAH UTS(MINGGU 7?)
--CARA BACKUP DATAFILE


SQL> --BACKUP DATA FILE --
SQL> --1. OFFLINE 
SQL> --a. backup ke disc yang sama 
SQL> --b. backup ke disk berbeda
SQL> --2. ONLINE
SQL>   
SQL> 
SQL> --skrng yg offline
SQL> --backup ke disk yang sama

SQL> select name from v$datafile;

NAME
--------------------------------------------------------------------------------
/home/oracle/app/oracle/oradata/orcl/system01.dbf
/home/oracle/app/oracle/oradata/orcl/sysaux01.dbf
/home/oracle/app/oracle/oradata/orcl/undotbs01.dbf
/home/oracle/app/oracle/oradata/orcl/users01.dbf
/home/oracle/app/oracle/oradata/orcl/example01.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_1930613455248703.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2041602962184952.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2610402357158758.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2611417663389985.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2614203650434107.dbf

10 rows selected.



SQL> --CEK DATAFILE YANG AKAN DIBACKUP BERADA DI TS MANA
SQL> SELECT FILE_NAME, TABLESPACE_NAME FROM DBA_DATA_FILES 
  2  WHERE TABLESPACE_NAME = 'EXAMPLE';

FILE_NAME
--------------------------------------------------------------------------------
TABLESPACE_NAME
------------------------------
/home/oracle/app/oracle/oradata/orcl/example01.dbf
EXAMPLE


SQL> SELECT STATUS FROM DBA_TABLESPACES WHERE TABLESPACE_NAME = 'EXAMPLE';

STATUS
---------
ONLINE

--TABELNYA DIALTER DULU BIAR OFFLINE

SQL> ALTER TABLESPACE EXAMPLE OFFLINE;

Tablespace altered.

SQL> 


SQL> !mkdir /home/oracle/backup_df

SQL> !cp /home/oracle/app/oracle/oradata/orcl/system01.dbf /home/oracle/backup_df
   

SQL> SQL> !ls /home/oracle/backup_df
system01.dbf

SQL> 

--MENGALTER DATABASE KEMBALI JADI ONLINE

SQL> ALTER TABLESPACE EXAMPLE ONLINE
  2  ;

Tablespace altered.

--DATABASE BERARTI SEKARANG SUDAH DIBACKUP.

--CONTOH PROSES RECOVER

SQL> 

SQL> SHUTDOWN ABORT;   
ORACLE instance shut down.

--DELETE FILE
SQL> !rm /home/oracle/app/oracle/oradata/orcl/system01.dbf;

SQL> startup force;
ORACLE instance started.

Total System Global Area  456146944 bytes
Fixed Size                  1344840 bytes
Variable Size             348129976 bytes
Database Buffers          100663296 bytes
Redo Buffers                6008832 bytes
Database mounted.
ORA-01157: cannot identify/lock data file 1 - see DBWR trace file
ORA-01110: data file 1: '/home/oracle/app/oracle/oradata/orcl/system01.dbf'


SQL> 

SQL> SHUTDOWN ABORT;   
ORACLE instance shut down.
SQL> !rm /home/oracle/app/oracle/oradata/orcl/system01.dbf;

SQL> startup force;
ORACLE instance started.

Total System Global Area  456146944 bytes
Fixed Size                  1344840 bytes
Variable Size             348129976 bytes
Database Buffers          100663296 bytes
Redo Buffers                6008832 bytes
Database mounted.
ORA-01157: cannot identify/lock data file 1 - see DBWR trace file
ORA-01110: data file 1: '/home/oracle/app/oracle/oradata/orcl/system01.dbf'


SQL> 
SQL> recover database;
Media recovery complete.
SQL> alter database open;

Database altered.


--UNTUK CEK
SQL> select file_name, tablespace_name from dba_data_files
  2  where file_name = '/home/oracle/app/oracle/oradata/orcl/system01.dbf';

FILE_NAME
--------------------------------------------------------------------------------
TABLESPACE_NAME
------------------------------
/home/oracle/app/oracle/oradata/orcl/system01.dbf
SYSTEM

SQL> --backup ke disk/lokasi yang berbeda
SQL> --buat direktori sebagai lokasi baru
SQL>        
SQL> !mkdir /home/oracle/new

SQL> select name from v$datafile;

NAME
--------------------------------------------------------------------------------
/home/oracle/app/oracle/oradata/orcl/system01.dbf
/home/oracle/app/oracle/oradata/orcl/sysaux01.dbf
/home/oracle/app/oracle/oradata/orcl/undotbs01.dbf
/home/oracle/app/oracle/oradata/orcl/users01.dbf
/home/oracle/app/oracle/oradata/orcl/example01.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_1930613455248703.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2041602962184952.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2610402357158758.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2611417663389985.dbf
/home/oracle/app/oracle/oradata/orcl/APEX_2614203650434107.dbf

10 rows selected.




--copy ke destinasi baru
SQL> !cp /home/oracle/backup_df/system01.dbf /home/oracle/new/system01.dbf; 

SQL> shutdown abort;
ORACLE instance shut down.
SQL> startup mount;


--menrename path lokasi nya ke database
SQL> alter database rename file
  2  '/home/oracle/app/oracle/oradata/orcl/system01.dbf'
  3  to
  4  '/home/oracle/new/system01.dbf';

Database altered.


SQL> select name from v$datafile;

NAME
--------------------------------------------------------------------------------
/home/oracle/new/system01.dbf



SQL> --online backup--
SQL> --status archive harus enabled
SQL> archive log list;
Database log mode              No Archive Mode
Automatic archival             Disabled
Archive destination            USE_DB_RECOVERY_FILE_DEST
Oldest online log sequence     615
Current log sequence           617


--MERUBAH MENJADI ENABLED
SQL> shutdown immediate;
Database closed.
Database dismounted.
ORACLE instance shut down.
SQL> startup mount;
ORACLE instance started.

Total System Global Area  456146944 bytes
Fixed Size                  1344840 bytes
Variable Size             348129976 bytes
Database Buffers          100663296 bytes
Redo Buffers                6008832 bytes
Database mounted.
SQL> alter database archivelog;

Database altered.



SQL> alter tablespace example begin backup;

Tablespace altered.


SQL> select * from v$backup;

     FILE# STATUS                CHANGE# TIME
---------- ------------------ ---------- ---------
         1 NOT ACTIVE                  0
         2 NOT ACTIVE                  0
         3 NOT ACTIVE                  0
         4 NOT ACTIVE                  0
         5 ACTIVE               14090405 15-APR-19
         6 NOT ACTIVE                  0
         7 NOT ACTIVE                  0
         8 NOT ACTIVE                  0
         9 NOT ACTIVE                  0
        10 NOT ACTIVE                  0

10 rows selected.


SQL> select name from v$datafile where file#=5;

NAME
--------------------------------------------------------------------------------
/home/oracle/app/oracle/oradata/orcl/example01.dbf



--MELALKUKAN PROSES BACKUP
SQL> !mkdir /home/oracle/barulagi

SQL> !cp /home/oracle/app/oracle/oradata/orcl/example01.dbf /home/oracle/barulagi

SQL> !ls /home/oracle/barulagi
example01.dbf

SQL> alter tablespace example end backup;

Tablespace altered.

SQL> 

SCN = SERIAL CHANGE NUMBER
SCN DIBUAT SAAT MELAKUKAN RECOVERY TABEL;


SQL> show user 
USER is "HR"
SQL> 



SQL> create table karyawan         
  2  (nik char(3), 
  3  nama varchar2(20),
  4  gaji number);

Table created.

SQL> insert into karyawan values('222','adit',100000);

1 row created.

SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                     100000

SQL> 

SQL> update karyawan set gaji = 3300;

1 row updated.

SQL> commit;

Commit complete.


SQL> update karyawan set gaji = 2000;

1 row updated


SQL> commit;

Commit complete.

SQL> update karyawan set gaji = 99999;

1 row updated.

SQL> commit;

Commit complete.


SQL> rollback
  2  ;

Rollback complete.

SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                      99999


--CARA UNTUK CEK SCN
SQL> SELECT * FROM karyawan VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE;


SQL> select current_scn from v$database;

CURRENT_SCN
-----------
   14091596



SQL> select scn_to_timestamp(14091596) from dual;

SCN_TO_TIMESTAMP(14091596)
---------------------------------------------------------------------------
15-APR-19 01.04.42.000000000 AM

SQL> 

SQL> select timestamp_to_scn('15-APR-19 01.04.42.000000000 AM') from dual;

TIMESTAMP_TO_SCN('15-APR-1901.04.42.000000000AM')
-------------------------------------------------
                                         14091595





SQL> select current_scn from v$database;

CURRENT_SCN
-----------
   14091711


SQL> select row_movement from user_tables where table_name = 'KARYAWAN';

ROW_MOVE
--------
DISABLED

SQL> alter table KARYAWAN ENABLE ROW MOVEMENT;

Table altered.


SQL> select row_movement from user_tables where table_name = 'KARYAWAN';

ROW_MOVE
--------
ENABLED



SQL> FLASHBACK TABLE KARYAWAN TO SCN 14091711;

Flashback complete.

SQL> select * FROM KARYAWAN;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                      99999

SQL> 

SQL> alter table karyawan disable row movement;

Table altered.

SQL> select row_movement from user_tables where table_name = 'KARYAWAN';

ROW_MOVE
--------
DISABLED


SQL> delete from karyawan;

1 row deleted.

SQL> rollback;

Rollback complete.

SQL> select * from karyawan;

NIK NAMA                       GAJI
--- -------------------- ----------
222 adit                      99999

SQL> truncate table karyawan;

Table truncated

SQL> rollback;   

Rollback complete.

SQL> select * from karyawan;

no rows selected


--pr cara flash back tabel yang dah di truncate pake scn