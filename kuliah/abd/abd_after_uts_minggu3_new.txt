DF:TUJUAN UNTUK MENYIMPAN DATA DAN TABLE 
SEMAKIN BANYAK DIBUAT MAKIN BANYAK STORED YANG TERPAKAI
TABLESPACE MINIMAL 1 DATAFILE


TABLESPACE STATUS :
ONLINE
OFFLINE
READ ONLY(BISA DIBACA TIDAK BISA DIUBAH/UPDATE)
READ WRITE(BISA DIBACA BISA DIUBAH)

DI HR
-SELECT * FROM REGIONS
select tablespace_name,status from dba_tablespaces;

ALTER TABLESPACE READ ONLY ;

SQL> select tablespace_name from user_tables
  2  where table_name='REGIONS';

SQL> UPDATE REGIONS
  2  SET REGION_NAME='INDONESIA'
  3  WHERE REGION_ID=5;

UNTUK ONLINE
ALTER TABLESPACE users online;


ALTER TABLESPACE USERS READ WRITE;
-SELECT * FROM REGIONS
(MAKA HASILNYA MASIH MUNCUL)

ALTER TABLESPACE USERS OFFLINE;
-SELECT * FROM REGIONS
(MAKA HASILNYA TIDAK ADA)

SQL> SELECT *FROM REGIONS;
SELECT *FROM REGIONS
             *
ERROR at line 1:
ORA-00376: file 4 cannot be read at this time
ORA-01110: data file 4: '/home/oracle/app/oracle/oradata/orcl/users01.dbf'

(--UNTUK ONLINE HARUS DI READ WRITE DULU )
ALTER TABLESPACE USERS READ WRITE;
ALTER TABLESPACE users online;
 
SQL> select tablespace_name,status from dba_tablespaces;

TABLESPACE_NAME                STATUS
------------------------------ ---------
SYSTEM                         ONLINE
SYSAUX                         ONLINE
UNDOTBS1                       ONLINE
TEMP                           ONLINE
USERS                          ONLINE
EXAMPLE                        READ ONLY
APEX_1930613455248703          ONLINE
APEX_2041602962184952          ONLINE
APEX_2610402357158758          ONLINE
APEX_2611417663389985          ONLINE
APEX_2614203650434107          ONLINE

TABLESPACE_NAME                STATUS
------------------------------ ---------
NEW_TS                         ONLINE
BIG_TS                         ONLINE

13 rows selected.



---MEMBUAT TABLESPACE
SQL> !mkdir /home/oracle/baru

SQL> create tablespace ts_baru
  2  DATAFILE '/home/oracle/baru/ts_baru.dbf'
  3  size 10M;


select tablespace_name,status from dba_tablespaces;
TABLESPACE_NAME                STATUS
------------------------------ ---------
SYSTEM                         ONLINE
SYSAUX                         ONLINE
UNDOTBS1                       ONLINE
TEMP                           ONLINE
USERS                          ONLINE
EXAMPLE                        ONLINE
APEX_1930613455248703          ONLINE
APEX_2041602962184952          ONLINE
APEX_2610402357158758          ONLINE
APEX_2611417663389985          ONLINE
APEX_2614203650434107          ONLINE

TABLESPACE_NAME                STATUS
------------------------------ ---------
NEW_TS                         ONLINE
BIG_TS                         ONLINE
TS_BARU                        ONLINE

14 rows selected.

SQL> SELECT TABLESPACE_NAME,FILE_NAME FROM DBA_DATA_FILES 
  2  WHERE TABLESPACE_NAME='TS_BARU';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
TS_BARU
/home/oracle/baru/ts_baru.dbf

SQL> --MENAMBAH DATA FILE KE DALAM TS
SQL> ALTER TABLESPACE TS_BARU
  2  ADD DATAFILE '/home/oracle/baru/ts_baru2.dbf'
  3  SIZE 10M;

SQL> SELECT TABLESPACE_NAME,FILE_NAME FROM DBA_DATA_FILES
  2  WHERE TABLESPACE_NAME='TS_BARU';
---MAKA TABLESPACE TERDAPAT 2

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
TS_BARU
/home/oracle/baru/ts_baru.dbf

TS_BARU
/home/oracle/baru/ts_baru2.dbf

SQL> --MERESIZE UKURAN DATAFILE
SQL> ADA 2 CARA :
1.MANUAL 
2.OTOMATIS(OTOMATIS BY SISTEM) HARUS DIDEFINISIKAN TAMBAHNYA BERAPA


SQL> --1.CARA MANUAL
SQL> ALTER DATABASE DATAFILE 
  2  '/home/oracle/baru/ts_baru.dbf'
  3  RESIZE 15M;

Database altered.


SQL> --UNTUK MENGECEK UKURANNYA
SQL> SELECT BYTES,AUTOEXTENSIBLE
  2  FROM DBA_DATA_FILES
  3  WHERE FILE_NAME='/home/oracle/baru/ts_baru.dbf';

     BYTES AUT
---------- ---
  15728640 NO
(AUTEWXTENSIBLE UNTUK MELIHAT AUTOMATIC ATAU TIDAK )
1 MB : 1JT BYTES

SQL> SELECT BYTES/(1024*1024) "MB", AUTOEXTENSIBLE
  2  FROM DBA_DATA_FILES
  3  WHERE FILE_NAME='/home/oracle/baru/ts_baru.dbf';

        MB AUT
---------- ---
        15 NO


--------------------------
SQL> SELECT BYTES/(1024*1024) "INITIAL SIZE",
  2  AUTOEXTENSIBLE,
  3  INCREMENT_BY,
  4  MAXBYTES/(1024*1024) "MAX SIZE"
  5  FROM dba_data_files
  6  where file_name='/home/oracle/baru/ts_baru.dbf';

INITIAL SIZE AUT INCREMENT_BY   MAX SIZE
------------ --- ------------ ----------
          15 YES          640        100

SQL> alter database datafile
  2  '/home/oracle/baru/ts_baru2.dbf'
  3  autoextend on
  4  next 10M
  5  MAXSIZE 10T;
alter database datafile
*
ERROR at line 1:
ORA-03206: maximum file size of (1342177280) blocks in AUTOEXTEND clause is out
of range



 SQL> --MEMBUAT BIG FILE TS  
SQL> CREATE BIGFILE TABLESPACE TS_BIG 
  2  DATAFILE '/home/oracle/baru/ts_big.dbf'
  3  size 10m;

Tablespace created.


SQL> select tablespace_name,bigfile from dba_tablespaces;

TABLESPACE_NAME                BIG
------------------------------ ---
SYSTEM                         NO
SYSAUX                         NO
UNDOTBS1                       NO
TEMP                           NO
USERS                          NO
EXAMPLE                        NO
APEX_1930613455248703          NO
APEX_2041602962184952          NO
APEX_2610402357158758          NO
APEX_2611417663389985          NO
APEX_2614203650434107          NO

TABLESPACE_NAME                BIG
------------------------------ ---
NEW_TS                         NO
BIG_TS                         YES
TS_BARU                        NO
TS_BIG                         YES

15 rows selected.

DATAFILE KITA :1.SMALL FILE (1,2 GB)
2. BIG FILE
KALAU TABLESPACE NYA BESAR BERARTI BIG FILE


SQL> ALTER DATABASE DATAFILE
  2  '/home/oracle/baru/ts_big.dbf'
  3  autoextend on
  4  NEXT 10M
  5  MAXSIZE 10T;

Database altered.

COBA UNTUK MASUKKAN BIG DATA KE DALAM TS_BIG2

SQL> ALTER TABLESPACE TS_BIG
  2  ADD DATAFILE '/home/oracle/baru/ts_big2.dbf'
  3  size 10M;
ALTER TABLESPACE TS_BIG
*
ERROR at line 1:
ORA-32771: cannot add file to bigfile tablespace
(TIDAK BISA ,KARENA 1 BIG DATA FILE HANYA PUNYA 1 DATAFILE)

DEFAULT DATABASE ,KALAU TIDAK DIDEFINISIKAN AKAN TARUH DI TABLESPACE 
SAAT CREATE DEFINISIKAN DI TABLESPACE MANA 


------DI TAB HR---------------

SQL> conn hr/oracle
Connected.
SQL> show user;
USER is "HR"
SQL> select default_tablespace from user_users;

DEFAULT_TABLESPACE
------------------------------
USERS

SQL> CREATE TABLE TESTER
  2  (KOLOM1 NUMBER);

Table created.

SQL> SELECT TABLESPACE_NAME FROM USER_TABLES    
  2  WHERE TABLE_NAME='TESTER';

TABLESPACE_NAME
------------------------------
USERS

------UNTUK UBAH KE TABLESPACES TS_BARU -------
SQL> CREATE TABLE TESTER2slee
  2  (KOLOM1 NUMBER,
  3  KOLOM2 NUMBER) TABLESPACE TS_BARU;

Table created.

SQL> SELECT TABLESPACE_NAME FROM USER_TABLES
  2  WHERE TABLE_NAME='TESTER2';

TABLESPACE_NAME
------------------------------
TS_BARU

SQL> 
-----------------------------------
SQL> --MEMINDAHKAN TS SEBUAH FILE KE TABLESPACE USERS 
SQL> ALTER TABLE TESTER2 MOVE TABLESPACE USERS;

Table altered.

SQL> 
SQL> SELECT TABLESPACE_NAME FROM USER_TABLES
  2  WHERE TABLE_NAME='TESTER2';

TABLESPACE_NAME
------------------------------
USERS


