SQL> --network file
SQL> --1. listener
SQL> --2. TNS(transparent network substrate)
SQL> 
--listener
--untuk mengakses database secara remote
-- isi : informasi db seperti nama lisener, port.
-- disimpan dalam listener.ora
-- lokasi /home/network/admin

[oracle@localhost ~]$ cd $ORACLE_HOME/network/admin
[oracle@localhost admin]$ ls
listener.ora  samples  shrept.lst  tnsnames.ora
[oracle@localhost admin]$ nano listener.ora
[oracle@localhost admin]$ 

--TO START
[oracle@localhost admin]$ lsnrctl start

LSNRCTL for Linux: Version 11.2.0.2.0 - Production on 13-MAY-2019 00:21:30

Copyright (c) 1991, 2010, Oracle.  All rights reserved.

TNS-01106: Listener using listener name LISTENER has already been started




--TO STOP
[oracle@localhost admin]$ lsnrctl stop

LSNRCTL for Linux: Version 11.2.0.2.0 - Production on 13-MAY-2019 00:23:07

Copyright (c) 1991, 2010, Oracle.  All rights reserved.

Connecting to (DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=0.0.0.0)(PORT=1521)))
The command completed successfully


--MEMBUAT LISTENER MENGGUNAKAN NETMGR(NETWORK MANAGER)
KETIK NETMGR DI TERMINAL

--CREATE NEW LISTENER


--1. KLIK ADD
--2. ISI LISTENING LOCATIONS
-------- ISI HOSTNYA (CEK DENGAN HOSTNAME)
-------- ISI PORT(SESUAI YANG TERSEDIA)
--3. ADD DATABASE SERVICES
-------- ISI GDN(CEK DENGAN SHOW PARAMETER DB_NAME) (GDN = VALUE)
-------- ISI SID
--4. di save

--start specific listener port
lsnrctl start NEW_LISTENE

SQL> --TNS
SQL> --tns: nama yang digunakan pada lokal komp untuk mengenali database server
SQL> --dibuat menggunakan NETCA(network configuration assistant)
SQL> 



SQL> 
SQL> --tahap 1:
SQL> --masuk ke NETCA:
SQL> --a. pilih listener configuration
SQL> --b. pilih local name
SQL> --c. klik finish
SQL> 



SQL> --tahap 2:
SQL> -- masuk netca:
SQL> --1. pilih local net service name configuration
SQL> --2. pilih add or create
SQL> --3. isi service name / global database name. cek dengan show parameter db_name
SQL> --4. pilih tcp/ip 
SQL> --5.isi hostname 
SQL> --6. isi port(listener yang tersedia atau sudah pernah dibuat)(liat portnya brp)
SQL> --7. isi nama TNS yang akan dipakai
SQL> --8. klik finish
SQL> 


SQL> 
SQL> --maintenance password sys 
SQL> --password sys hanya dibutuhkan jika sys koneksi ke db secara remote
SQL> --koneksi secara lokal tidak butuh password
SQL>   
SQL> --cek user list






SQL> col username format a10;
SQL> col machine format a15;
SQL> select username, machine,sid,serial# from v$session where username is not null;

USERNAME   MACHINE                SID    SERIAL#
---------- --------------- ---------- ----------
APEX_PUBLI localhost.local         23         11
C_USER     domain

SYS        localhost.local         32         27
           domain

APEX_LISTE localhost.local         38          5
NER        domain

SYS        localhost.local         51        103
           domain

USERNAME   MACHINE                SID    SERIAL#
---------- --------------- ---------- ----------


SQL> --buat kill session orang
SQL> alter system kill session '51,103';



[oracle@localhost ~]$ cd $ORACLE_HOME/dbs
[oracle@localhost dbs]$ ls
hc_DBUA0.dat  init.ora      lkORCL     spfileorcl.ora
hc_orcl.dat   initorcl.ora  orapworcl
[oracle@localhost dbs]$ cat orapworcl
B�<�v�57�f��P��SYS8A8F025737A9097A�����ø�E+�oL�%41y)z��j�1Q��|�
                                                                           BSYSTEM2D594E86F93B17A1��w�GZ6����
                                  ��5
���>Tcw�Vc��[oracle@localhost dbs]$ nano orapworcl


[oracle@localhost dbs]$ mv orapworcl mypasssword
[oracle@localhost dbs]$ cat orapworcl
cat: orapworcl: No such file or directory
[oracle@localhost dbs]$ ls
hc_DBUA0.dat  init.ora      lkORCL       spfileorcl.ora
hc_orcl.dat   initorcl.ora  mypasssword
[oracle@localhost dbs]$ cat mypassword
cat: mypassword: No such file or directory
[oracle@localhost dbs]$ cat my passsword
cat: my: No such file or directory
cat: passsword: No such file or directory
[oracle@localhost dbs]$ ls
hc_DBUA0.dat  init.ora      lkORCL       spfileorcl.ora
hc_orcl.dat   initorcl.ora  mypasssword
[oracle@localhost dbs]$ rm -rf mypasssword
[oracle@localhost dbs]$ ls
hc_DBUA0.dat  hc_orcl.dat  init.ora  initorcl.ora  lkORCL  spfileorcl.ora
[oracle@localhost dbs]$ 



[oracle@localhost dbs]$ orapwd file=orapworcl password=newpass







[oracle@localhost dbs]$ sqlplus sys/oracle@orcl as sysdba;

SQL*Plus: Release 11.2.0.2.0 Production on Mon May 13 01:28:50 2019

Copyright (c) 1982, 2010, Oracle.  All rights reserved.

ERROR:
ORA-01017: invalid username/password; logon denied


Enter user-name:         
[oracle@localhost dbs]$ sqlplus sys/newpass@orcl as sysdba;

SQL*Plus: Release 11.2.0.2.0 Production on Mon May 13 01:29:08 2019

Copyright (c) 1982, 2010, Oracle.  All rights reserved.


Connected to:
Oracle Database 11g Enterprise Edition Release 11.2.0.2.0 - Production
With the Partitioning, OLAP, Data Mining and Real Application Testing options

SQL> 
