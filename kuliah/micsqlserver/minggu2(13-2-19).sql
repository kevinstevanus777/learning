create database perpustakaan


create table buku(
id_buku varchar(10),
judul varchar(50) not null,
penulis varchar(50),
kategori varchar(10),
edisi varchar(5),
penerbit varchar(50),
constraint pk_buku primary key(id_buku)
)


create table status(
id_status varchar(2),
jumlah_buku int,
batas_pinjam int,
nama_status varchar(20),
constraint pk_status primary key(id_status))


create table peminjam(
id_peminjam varchar(15),
id_status varchar(2),
nama_peminjam varchar(20),
nim varchar(15),
constraint pk_peminjam primary key(id_peminjam),
constraint fk_peminjam foreign key(id_status) references status(id_status))


create table pinjaman(
id_buku varchar(10),
id_peminjam varchar(15),
tgl_pinjam date,
tgl_kembali date,
constraint pk_pinjaman primary key(id_buku,id_peminjam),
constraint fk_pinjaman1 foreign key(id_buku) references buku(id_buku),
constraint fk_pinjaman2 foreign key(id_peminjam) references peminjam(id_peminjam))


alter table pinjaman add denda

create or replace view untuk cek buku apa aja yg dipinjam beserta waktunya secara
 real time serta kasih liat yang udah minus waktu pinjamnya
