create table dosen(
NIK varchar2(5) primary key,nama_dosen varchar2(25),alamat varchar2(30));


create table prog_studi(
KD_PS number primary key, nama_progstudi varchar2(25));


create table matakuliah(
KD_MTK varchar2(5) primary key, nama_mtk varchar2(25), sks number);

create table nilai(
NIM varchar2(8), kd_mtk varchar(5), kelas char(2), nilai number, 
constraint PK_nilai primary key(NIM,kd_mtk,kelas));



create table mahasiswa(
NIM varchar2(8) primary key, 
nama_mahasiswa varchar2(25), 
tanggal_lhr date,
KD_PS number,
dosen_pembimbing varchar(5),
constraint fk_kd_ps foreign key(KD_PS) references prog_studi(KD_PS),
constraint fk_dosen_pembimbing foreign key(dosen_pembimbing) references dosen(NIK));

create table jadwal(
KD_MTK varchar2(5),
kelas char(2),
NIK varchar2(5),
hari varchar2(10),
constraint pk_jadwal primary key(KD_MTK,kelas),
constraint fk_jadwal foreign key(NIK) references dosen(NIK));


insert into dosen(NIK,nama_dosen,alamat) values(10401,'Agus Susilo','Jl. Mangga');
insert into dosen(NIK,nama_dosen,alamat) values(10402, 'Dewi Agustin' ,'Jl. Mawar' );
insert into dosen(NIK,nama_dosen,alamat) values(10403, 'Yuliana' ,'Jl. Delima');
insert into dosen(NIK,nama_dosen,alamat) values(10501, 'Julius' ,'Jl. Durian');
insert into dosen(NIK,nama_dosen,alamat) values(10502, 'Husni Agustinus', 'Jl. Anggrek');
insert into dosen(NIK,nama_dosen,alamat) values(10503, 'Fary Muhamad', 'Jl. Semangka');


--not yet boiz
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (82507001, 'Denny', '09 Jan 1988',2 ,10401);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (82507002, 'Rina', '10 Aug 1987', 2, 10401);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (82507003, 'Winsy', '11 Dec 1988', 2, 10402);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (82507006, 'Bagas', '19 Sep 1987', 2, 10402);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (53507010, 'Ginna', '24 Jan 1986', 1, 10501);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (53507011, 'Yanto', '05 Jun 1986', 1, 10501);
insert into mahasiswa(NIM,NAMA_MAHASISWA,tanggal_lhr,KD_PS,dosen_pembimbing) values (53507013, 'Deswita', '15 Sep 1987', 1, 10501);

insert into prog_studi(KD_PS,nama_progstudi) values (1,'Teknik Informatika');
insert into prog_studi(KD_PS,nama_progstudi) values (2, 'Sistem Informasi');


insert into matakuliah(kd_mtk, nama_mtk,sks) values('TK001', 'Algoritma I', 2);
insert into matakuliah(kd_mtk, nama_mtk,sks) values('TK002', 'Sistem Informasi', 3);
insert into matakuliah(kd_mtk, nama_mtk,sks) values('TK003', 'Rekaya Perangkat Lunak', 4);
insert into matakuliah(kd_mtk, nama_mtk,sks) values('TK004', 'Metodologi Survey', 2);
insert into matakuliah(kd_mtk, nama_mtk,sks) values('TK005', 'E-Commerce', 3);


insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK001 A 10401 Senin);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK001 B 10402 Rabu);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK003 A 10501 Senin);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK004 A 10502 Rabu);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK004 B 10403 Kamis);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK005 B 10503 Jumat);
insert into jadwal(kd_mtk,kelas,NIK,hari) values(TK005 C 10503 Selasa);


insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK001', 'A',10401,'Senin');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK001', 'B',10402,'Rabu');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK003', 'A',10501,'Senin');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK004', 'A',10502,'Rabu');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK004', 'B',10403,'Kamis');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK005', 'B',10503,'Jumat');
insert into jadwal(kd_mtk,kelas,NIK,hari) values('TK005', 'C',10503,'Selasa');


insert into nilai(NIM,kd_mtk,kelas,nilai) values(82507001,'TK001','A',72);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(82507001,'TK003','A',81);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(82507003,'TK001','B',75);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(82507003,'TK003','A',57);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(53507010,'TK005','C',78);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(53507010,'TK003','A',66);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(53507013,'TK004','B',56);
insert into nilai(NIM,kd_mtk,kelas,nilai) values(53507013,'TK001','B',85);	



--no3
select NIM, NAMA_MAHASISWA,dosen.NAMA_DOSEN
from mahasiswa
right outer join dosen
on mahasiswa.dosen_pembimbing = dosen.NIK
order by mahasiswa.NIM 

--no4
select m.KD_PS, ps.NAMA_PROGSTUDI, count(m.nim)
from mahasiswa m,prog_studi ps
where m.KD_PS = ps.KD_PS
group by m.KD_PS, ps.nama_progstudi

--no5
select m.KD_MTK,m.nama_mtk,max(n.nilai)
from matakuliah m,nilai n
where n.KD_MTK = m.KD_MTK
group by m.KD_MTK,m.NAMA_MTK

--no6 done
select d.nik "NIK", d.NAMA_DOSEN "Nama Dosen"
from dosen d, jadwal j
where d.nik = j.nik AND j.hari= 'Senin';

--no7
select m.nim, m.nama_mahasiswa, n.nilai
from mahasiswa m,nilai n,matakuliah mtk
where m.nim=n.nim 
    AND mtk.KD_MTK = n.kd_mtk 
    AND mtk.nama_mtk = 'Algoritma I' 
    AND n.nilai > 
            (select n.nilai 
            from mahasiswa m , nilai n,matakuliah mtk  
            where m.nama_mahasiswa = 'Denny' 
                AND m.nim=n.nim 
                AND mtk.KD_MTK = n.kd_mtk 
                AND mtk.nama_mtk = 'Algoritma I');

--no8
alter table nilai
add keterangan varchar2(5);

--no9
select count(*) "Total_mhs",
count(case when to_char(TANGGAL_LHR,'YYYY') = 1986  then 1 end) "1986",
count(case when to_char(TANGGAL_LHR,'YYYY') = 1987  then 1 end) "1987",
count(case when to_char(TANGGAL_LHR,'YYYY') = 1988  then 1 end) "1988" 
from mahasiswa;

--no10 
select NAMA_MAHASISWA, tanggal_lhr 
from mahasiswa
where TANGGAL_LHR > (select tanggal_lhr from mahasiswa where nama_mahasiswa = 'Rina') 
AND NAMA_MAHASISWA != 'Rina'