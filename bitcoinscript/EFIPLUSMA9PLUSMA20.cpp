//ELDERFORCEINDEX+MA9+MA20

study(title="Elder's Force Index + MA9 + MA20", shorttitle="EFI+MA9+MA20")

//deklarasi var length_long dengan source yaitu input bernilai 13 
length_long = input(13, minval=1, title="EFI")                       


//deklarasi var efi_long dengan source yaitu dari ema(perubahan(terakhir) * volume, length_long)
efi_long = ema(change(close) * volume, length_long)     //efi

//output ke grafik dengan source efi_long
plot(efi_long, color=red, title="EFI_long")             //output efi

//endofelderforceindex



//MA

//deklarasi var len1 dengan input sebesar 9
len1 = input(9, minval=1, title="Length")

//var source
src1 = ema(change(close) * volume, length_long)

//deklarasi var out1
out1 = sma(src1, len1)

//output ke grafik dengan source dari var out1
plot(out1, color=blue, title="MA9")

//endof MA


//MA20

//deklarasi var len1 dengan input sebesar 9
len2 = input(20, minval=1, title="Length")

//var source
src2 = ema(change(close) * volume, length_long)

//deklarasi var out1
out2 = sma(src2, len2)

//output ke grafik dengan source dari var out1
plot(out2, color=green, title="MA20")

//endof MA



