//SupportAndResistance_test5

//@version=1
// This is an updated version of my previous Kay_SRLevels Support and Resistance (Barry) indicator. 
// If this indicator have helped you in your trading or you plan to use the code in one of your script,

// please donate to following with a message to specify the money as Donation.
// SKRILL : trading.kay27@gmail.com

// Donated money will go to PlanCanada and other charaties around the world.

// LETS PAY THE TAXES FROM OUR TRADING PROFITS TO NATURE, COMMUNITY AND GOOD CAUSES :smiley: 

study("Dynamic SR(Fractals)",shorttitle="Kay_DSRFractals", overlay=true)

high4=nz(high[4])
high3=nz(high[3])
high2=nz(high[2])
high1=nz(high[1])

low4 = nz(low[4])
low3 = nz(low[3])
low2 = nz(low[2])
low1 = nz(low[1])

dnFractal = high2 >= high3 and high2 >= high4 and high2 >= high1 and high2 >= high ? high2 : fixnan(dnFractal[1])
upFractal = low2 <= low3 and low2 <= low4 and low2 <= low1 and low2 <= low ? low2 : fixnan(upFractal[1])

// Plot the fractals as shapes on the chart.
plot(dnFractal, title="D", style=circles, color=blue, transp=10) // Down Arrow above candles
plot(upFractal, title="U", style=circles, color=orange, transp=10)  // Up Arrow below candles