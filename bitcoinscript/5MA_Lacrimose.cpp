//5MAlacrimose

study(title="Moving Average", shorttitle="MA", overlay=true)
len1 = input(9, minval=1, title="Length")
src1 = input(close, title="Source")
out1 = sma(src1, len1)
plot(out1, color=blue, title="MA")

len2 = input(20, minval=1, title="Length")
src2 = input(close, title="Source")
out2 = sma(src2, len2)
plot(out2, color=red, title="MA")

len3 = input(50, minval=1, title="Length")
src3 = input(close, title="Source")
out3 = sma(src3, len3)
plot(out3, color=green, title="MA")


len4 = input(100, minval=1, title="Length")
src4 = input(close, title="Source")
out4 = sma(src4, len4)
plot(out4, color=purple, title="MA")

len5 = input(150, minval=1, title="Length")
src5 = input(close, title="Source")
out5 = sma(src5, len5)
plot(out5, color=yellow, title="MA")

len6 = input(200, minval=1, title="Length6")
src6 = input(close, title="Source")
out6 = sma(src6, len6)
plot(out6, color=red, title="MA")