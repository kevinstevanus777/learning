//@version=2
study("TD Sequential+5MA_Lacrimose",overlay=true)
transp=input(0)


// SETUP
a = nz(sellsetup[1]) + 1
sellsetup = close > close[4] ? (a > 9 ? 1 : a) : 0

b = nz(buysetup[1]) + 1
buysetup = close < close[4] ? (b > 9 ? 1 : b) : 0

plotchar(sellsetup == 1 ? true : na, char="1", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 2 ? true : na, char="2", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 3 ? true : na, char="3", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 4 ? true : na, char="4", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 5 ? true : na, char="5", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 6 ? true : na, char="6", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 7 ? true : na, char="7", color=green, location=location.abovebar, transp=transp)
plotchar(sellsetup == 8 ? true : na, char="8", color=green, location=location.abovebar, transp=transp)
plotshape(sellsetup == 9 ? true : na, text="9", style=shape.arrowdown, textcolor=green, color=red, location=location.abovebar, transp=transp)

plotchar(buysetup == 1 ? true : na, char="1", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 2 ? true : na, char="2", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 3 ? true : na, char="3", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 4 ? true : na, char="4", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 5 ? true : na, char="5", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 6 ? true : na, char="6", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 7 ? true : na, char="7", color=red, location=location.abovebar, transp=transp)
plotchar(buysetup == 8 ? true : na, char="8", color=red, location=location.abovebar, transp=transp)
plotshape(buysetup == 9 ? true : na, text="9", style=shape.arrowup, textcolor=red, color=lime, location=location.abovebar, transp=transp)

// COUNTDOWN
buy_countdown() => 
    currentcount = 0
    count = 0
    for i = 0 to 100
        count := count + (close[i] < low[i + 2] ? 1 : 0)
        if sellsetup[i] == 9 or count > 13
            break
        currentcount := buysetup[i] == 9 ? count : currentcount
    currentcount

sell_countdown() => 
    currentcount = 0
    count = 0
    for i = 0 to 100
        count := count + (close[i] > high[i + 2] ? 1 : 0)
        if buysetup[i] == 9 or count > 13
            break
        currentcount := sellsetup[i] == 9 ? count : currentcount
    currentcount

aggressive_buy() =>
    isaggressivebuy = false
    count = 0
    for i = 0 to 50
        count := count + (low[i] < low[i + 2] ? 1 : 0)
        if sellsetup[i] == 9 or count > 13
            break
        if buysetup[i] == 9 and count == 13
            isaggressivebuy := true
            break
    isaggressivebuy

aggressive_sell() =>
    isaggressivesell = false
    count = 0
    for i = 0 to 50
        count := count + (high[i] > high[i + 2] ? 1 : 0)
        if buysetup[i] == 9 or count > 13
            break
        if sellsetup[i] == 9 and count == 13
            isaggressivesell := true
            break
    isaggressivesell

buycountdown = close < low[2] ? buy_countdown() : 0
sellcountdown = close > high[2] ? sell_countdown() : 0
aggressivebuy = low < low[2] ? aggressive_buy() : false
aggressivesell = high > high[2] ? aggressive_sell() : false
combobuy = false
combosell = false

plotchar(buycountdown == 1 ? true : na, char="", text="\n1", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 2 ? true : na, char="", text="\n2", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 3 ? true : na, char="", text="\n3", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 4 ? true : na, char="", text="\n4", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 5 ? true : na, char="", text="\n5", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 6 ? true : na, char="", text="\n6", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 7 ? true : na, char="", text="\n7", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 8 ? true : na, char="", text="\n8", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 9 ? true : na, char="", text="\n9", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 10 ? true : na, char="", text="\n10", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 11 ? true : na, char="", text="\n11", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 12 ? true : na, char="", text="\n12", textcolor=red, location=location.belowbar, transp=transp)
plotchar(buycountdown == 13 ? true : na, char="", text="\n13", textcolor=red, location=location.belowbar, transp=transp)
plotshape(buycountdown == 13 ? true : na, style=shape.arrowup, color=lime, textcolor=red, location=location.belowbar, transp=transp)
plotshape(aggressivebuy ? true : na, style=shape.arrowup, text="A", color=lime, textcolor=red, location=location.belowbar, transp=transp)
plotshape(combobuy ? true : na, style=shape.arrowup, text="\nC", color=lime, textcolor=red, location=location.belowbar, transp=transp)

plotchar(sellcountdown == 1 ? true : na, char="", text="\n1", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 2 ? true : na, char="", text="\n2", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 3 ? true : na, char="", text="\n3", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 4 ? true : na, char="", text="\n4", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 5 ? true : na, char="", text="\n5", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 6 ? true : na, char="", text="\n6", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 7 ? true : na, char="", text="\n7", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 8 ? true : na, char="", text="\n8", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 9 ? true : na, char="", text="\n9", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 10 ? true : na, char="", text="\n10", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 11 ? true : na, char="", text="\n11", textcolor=green, location=location.belowbar, transp=transp)
plotchar(sellcountdown == 12 ? true : na, char="", text="\n12", textcolor=green, location=location.belowbar, transp=transp)
plotshape(sellcountdown == 13 ? true : na, style=shape.arrowdown, text="\n\n13", color=red, textcolor=green, location=location.belowbar, transp=transp)
plotshape(aggressivesell ? true : na, style=shape.arrowdown, text="A", color=red, textcolor=green, location=location.belowbar, transp=transp)
plotshape(combosell ? true : na, style=shape.arrowdown, text="\nC", color=red, textcolor=green, location=location.belowbar, transp=transp)




//@version=3
//ma
len1 = input(9, minval=1, title="Length")
src1 = input(close, title="Source")
out1 = sma(src1, len1)
plot(out1, color=blue, title="MA")

len2 = input(20, minval=1, title="Length")
src2 = input(close, title="Source")
out2 = sma(src2, len2)
plot(out2, color=red, title="MA")

len3 = input(50, minval=1, title="Length")
src3 = input(close, title="Source")
out3 = sma(src3, len3)
plot(out3, color=green, title="MA")


len4 = input(100, minval=1, title="Length")
src4 = input(close, title="Source")
out4 = sma(src4, len4)
plot(out4, color=purple, title="MA")

len5 = input(150, minval=1, title="Length")
src5 = input(close, title="Source")
out5 = sma(src5, len5)
plot(out5, color=yellow, title="MA")

len6 = input(200, minval=1, title="Length6")
src6 = input(close, title="Source")
out6 = sma(src6, len6)
plot(out6, color=black, title="MA")
