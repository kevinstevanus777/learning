#include <bits/stdc++.h>
#define REP(i,n) for (int i = 0; i < (n); ++i)


using namespace std;


struct Node
  {
  	int angka;
  	int pangkat;
    Node *next;
  };	
  
void InsertNode(Node **L, int angka, int pangkat){
	Node *baru;
	baru = new Node;
	baru -> angka = angka;
	baru -> pangkat = pangkat;
	baru -> next = NULL;
	if(*L == NULL){
		*L = baru;
	}

	else{
		Node *t;
		t = *L;
		while(t-> next	!= NULL){
			 t= t-> next;
		}
		t-> next = baru;
	}

}


void PrintList(Node *L){
	Node *n;
	n = L;
	while(n-> next != NULL){
		cout << n-> angka << "x" << n-> pangkat<< " + ";
		n = n-> next;
	}

	if(n-> next == NULL){
		cout << n-> angka<< "x" << n-> pangkat << endl;
	}
}

void penjumlahan(Node *P1, Node *P2){
	Node *a, *b;
	a = P1;
	b =  P2;


	Node *hasil;
	hasil = NULL;
	


	while(true){
		if(a->pangkat < b-> pangkat){
			InsertNode(&hasil, a->angka, a->pangkat);
			a = a->next;
		}		
		else if( a->pangkat > b-> pangkat){
			InsertNode(&hasil, b->angka,b->pangkat);
			b = b->next;
		}
		else if(a->pangkat == b-> pangkat){
			a->angka = a->angka+ b->angka;
			InsertNode(&hasil,a->angka, a->pangkat );
			a = a->next;
			b = b->next;
		}

		if(a == NULL && b == NULL){
			break;
		}
	}

	while(hasil->next != NULL){
		PrintList(hasil);
		hasil = hasil->next;
	}


}



int main(){
	ios_base::sync_with_stdio(false); /*cin.tie(0);*/
	

	Node *P1, *P2;
	P1 = P2 = NULL;

	for(int i =1;i<3;i++){
		int n;
		cout << "ketik banyak suku dalam poli " << i << " = ";
		cin >> n ;
		cout << endl;

		for(int j=1; j <= n;j++){
			int angka, pangkat;
			cout << "Masukkan konstanta untuk suku ke " << j << " = ";
			cin >> angka;
			cout << "Masukkan pangkat untuk suku ke " << j << " = ";
			cin >> pangkat;

			if(i == 1){
				InsertNode(&P1, angka, pangkat);
			}
			else{
				InsertNode(&P2, angka, pangkat);
			}
			cout << endl;
		}
		cout << endl;
	}
	
	cout << "Persamaan 1" << endl;
	PrintList(P1);

	cout << "Persamaan 2" << endl;
	PrintList(P2);

	cout << "Hasil penjumlahan = "<< endl;
	penjumlahan(P1, P2);
	

}
