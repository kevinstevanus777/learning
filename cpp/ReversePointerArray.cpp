//NO5 ALGO!


/*Buatlah fungsi yang menerima array integer lalu mengkopi array tersebut sehingga elemen array hasil kopi mempunyai urutan kebalikan dari elemen array semula.
 Fungsi menghasilkan pointer ke array hasil kopi. Buat program utamanya. Banyaknya elemen array mula-mula ditentukan sendiri*/

#include<iostream>

using namespace std;

void arraybaru(int *array, int sizeofarray);

int main()
{
	cout << "Nama = Kevin Stevanus\n" << "NIM = 825170005\n\n";		
	int *array;		//deklarasi integer pointer dengan nama variabel 'array'
	int sizeofarray;	//deklarasi variabel sizeofarray yaitu besarnya jumlah array

	//input besar jumlah array
	cout << "Masukkan sizeofarray = ";
	cin >> sizeofarray;

	//deklarasi variabel array memiliki besar array sebesar sizeofarray
	array = new int [sizeofarray];

	//input elemen array ke var array
	for(int i = 0; i<sizeofarray;i++){
		cout << "Masukkan elemen array ke-"<<i+1 << " = ";
		cin >> array[i];
	} 

	arraybaru(array,sizeofarray);
	

	
}



void arraybaru(int *array, int sizeofarray){
	//deklarasi variabel newarray dengan jumlah elemen array sebesar sizeofarray
	int *newarray;	//deklaras variabel newarray untuk reversal elemen dari array lama
	
	newarray = new int [sizeofarray];

	//membuat seluruh value dari newarray menjadi 0
	for(int i = 0;i<sizeofarray;i++){
		newarray[i] = 0;
	}

	//memasukkan elemen oldarray ke var newarray
	int j = sizeofarray;
	for(int i = 0;i<sizeofarray;i++){
		
		newarray[i] = array[j-1];	// 'i' diambil sebagai starter untuk newarray; sedangkan j digunakan sebagai reversal
		--j;
	}

	//output
	for(int i = 0;i<sizeofarray;i++){
		cout << "Newarray-"<<i+1<<" = "<<newarray[i]<< endl;
	}
}
