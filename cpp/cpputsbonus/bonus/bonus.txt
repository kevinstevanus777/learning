#include <bits/stdc++.h>
#define endl "\n"
#define REP(i,n) for (int i = 0; i < (n); ++i)
#define REPD(i,n) for (int i = (n)-1; 0 <= i; --i)
#define BUG(x) cout<< '>' << #x << ':' <<x<<endl

using namespace std;

//declar char global
char input[500] = {'\0'};

//strlen[global]
int length;

//reference on int(main)
int &l = length;

//done
void del(){

	cout << "Kata yang akan dihapus : ";
	
	char hapus[50] = {'\0'};	//declare var untuk hapus
	
	cin.ignore();	//flush
	
	cin.getline(hapus,sizeof(hapus));	//input
	
	char output[500] = {'\0'};	//declare output
	
	int temp = 0; //i++


	int panjanghapus = strlen(hapus);	//limiter

	REP(i,length){//outer loop
		REP(j,panjanghapus){//inner loop
			if(hapus[j] == input[i]){//jika string hapus ditemukan di string input
				input[i] = '\0';	//isi menjadi null
			}
			else{
				output[temp] = input[i];	//else, masuk ke output
				temp++;						//i++
			}
		}
	}
		
	//output	
	cout << "HASIL = ";
	REP(i,length){
		cout << output[i];
	}
	cout << endl;
}

//done
void insert(){

	cout << "Urutan : ";

	//stdin nomor URUTAN
	int posinsert;
	cin >> posinsert;

	//stdin CHAR URUTAN
	cout << "Insert: ";
	char whattoinsert[500] = {'\0'};
	cin.ignore();
	cin.getline(whattoinsert,sizeof(whattoinsert));
	int lengthwhattoinsert = strlen(whattoinsert);

	//output
	char output[500] = {'\0'};
	bool ding = false;
	int temp = 0;
	REP(i,length){
		if(i ==  posinsert){
			ding = true;
			REP(j,lengthwhattoinsert){
				output[temp] = whattoinsert[j];
				temp++;
			}
			output[temp] =  input[i];
			temp++;
		}
		else{

		output[temp] = input[i];
		temp++;
		}
	}
	cout << endl;
	cout << "Output : " << output << endl;
}


/*void find(){
	cout << "Kata yang dicari : ";
	// char dicari[500]; //declare
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	string dicari;
	
	//stdin
	// cin.getline(dicari,sizeof(dicari));
	// int dicarilength = strlen(input), lengthdicari = strlen(dicari);
	getline(cin, dicari);	
	string output(input);
	REP(i,length){
	if(size_t index = output.find(dicari)){
		cout << "found at = " << index-1<< endl;
	}
		
	}

}*/

//nyerah
void find(){
	cout << "Kata yang dicari: ";
	char dicari[500];
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	cin.getline(dicari,sizeof(dicari));

	int n = strlen(input);
	int m = strlen(dicari);

	int s,j;
	for(s = 0;s<=n-m;s++){
		for(j = 0;j<= m;j++){
			if(dicari[j]!=input[s+j])break;
		}
		if(j == m){
			cout <<"found at: "<< s  << endl;
		}
	}

}

void replace(){
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	//INPUT YANG INGIN DIGANTI
	cout << "Kata yang ingin diganti : ";
	// cin.clear();
	char yangingindiganti[500] = {'\0'};
	cin.getline(yangingindiganti,sizeof(yangingindiganti));
	int n = strlen(yangingindiganti);


	//INPUT PENGGANTI
	cout << "Kata yang menggantikan : ";
	char diganti[500] = {'\0'};
	cin.getline(diganti,sizeof(diganti));
	// cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	int m = strlen(diganti);

	int pos = 0;
	string output(input);
	
	int s,j;
	for(s=0;s<=length-n;s++){
		for(j=0;j<n;j++){
			if(yangingindiganti[j] != input[s+j]) break;
		}
		if(j == m ){
			/*int dummy = s;
			REP(i,m){

			}*/
			output.replace(s,m,diganti);
		}
	}
	BUG(output);
/*	BUG(yangingindiganti);
	BUG(diganti);
	BUG(n);
	BUG(m);*/

/*	REP(i,length){
		//cek index pertama
		if(input[i] == diganti[0]){
			//loop j sampai string length yang ingin diganti
			REP(j,n){
				//jika input != yang ingindiganti; break;
				if(input[j]!=yangingindiganti[j]){
					break;
				}
				else{
					REP(x,m){
						input[x] = diganti[x];
					}
				}
			}
		} 
	}*/
/*	bool ding = true;
	REP(i,length){
		if(input[i] == yangingindiganti[0]){
			REP(j,n){
				if(input[j] != yangingindiganti[j]){
					break;
				}
				else{
					BUG(j);
				}
			}
		}
	}*/

	//BUG(input);



}

int main()
{
	ios_base::sync_with_stdio(false); //cin.tie(0);
	cout << "Input string = ";
	cin.getline(input,sizeof(input));
	cout << endl;

	l = strlen(input);


cout << endl << endl;
	int input = 0;
	while(true){
		cout << "1. Find\n2. Replace\n3. Insert\n4. Del\n5. Quit\nAnswer = ";
		cin >> input;

		if(input == 1 ){
			find();
		}
		else if(input == 2){
			replace();
		}
		else if(input == 3){
			insert();
		}
		else if(input == 4){
			del();
		}
		else{
			break;
		}
	}
}
