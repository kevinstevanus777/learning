#include <bits/stdc++.h>
#define endl "\n"
#define REP(i,n) for (int i = 0; i < (n); ++i)
#define REPD(i,n) for (int i = (n)-1; 0 <= i; --i)
#define BUG(x) cout<< '>' << #x << ':' <<x<<endl
// #define IN() freopen("a.txt", "r", stdin)

using namespace std;

//declare struct LL
struct Node{
	int nomor;
	int point;
	char nama[30];
	Node *next;
};

int best = 0;
int &be = best;

void InsertNode(Node **L, int nomor, int point, char nama[]){
	Node *baru;
	baru = new Node;
	baru -> nomor = nomor;
	baru -> point = point;
	strcpy(baru->nama,nama);
	baru -> next = NULL;
	if(*L == NULL){
		*L = baru;
	}
	else{
		Node *t;
		t = *L;
		while(t -> next != NULL){
			t = t-> next;
		}
		t-> next = baru;
	}
}

void PrintList(Node *L){
	Node *n;
	n = L;
		cout << setw(30) << "NAMA" << setw(8) << "NOMOR" << setw(8) << "POINT" << endl << endl;	
	while(n-> next != NULL){
		// cout <<"nama = " << n-> nama << "nomor = " <<  n->nomor << endl << "point = " << n->point << endl << endl;
		cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;
		n = n->next;
	}

	if(n-> next == NULL){
		cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;

	}
}

void Hitung(Node *L){
	Node *n;
	n = L;
		cout << setw(30) << "NAMA" << setw(8) << "NOMOR" << setw(8) << "POINT" << endl << endl;	
	/*while(n-> next != NULL && n->point == be){
		cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;
		n = n->next;

		if(n-> next == NULL && n->point == be){
		
			cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;break;
		
		}
	}*/

	while(n->next!= NULL){
		if(n->point == best){
		cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;
		n = n->next;		
		}
		else{
			n=n->next;
		}
	}

	
	if(n->next == NULL){
		if(n->point == best){
		cout << setw(30) << n->nama << setw(8) << n->nomor << setw(8) << n->point << endl;
		}
		// break;
	}


}

int main()
{
	ios_base::sync_with_stdio(false); //cin.tie(0);

	//chain linked list
	Node *Linkedlist;
	Linkedlist = NULL;

	Node *nomor, *point;
	nomor = point = NULL;
	int jumlah = 2;
	REP(i,jumlah){
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		int nomor, point;
		char nama[30] = {'\0'};
		cout << "Input nama: ";
		cin.getline(nama,sizeof(nama));
		// cin.ignore(256, '\n');
		cout << "Input nomor: ";
		cin >> nomor;
		cout << "Input point: ";
		cin >> point; 
		if(point>be){
			be = point;
		}
		InsertNode(&Linkedlist, nomor, point, nama);	
	}
	cout << endl;
	// BUG(be);
	cout << "\nPrintList\n";
	PrintList(Linkedlist);
	cout << "\nTerbesar:\n";
	Hitung(Linkedlist);
}
