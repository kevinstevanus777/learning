#include <bits/stdc++.h>
#define endl "\n"
#define REP(i,n) for (int i = 0; i < (n); ++i)
#define REPD(i,n) for (int i = (n)-1; 0 <= i; --i)
#define BUG(x) cout<< '>' << #x << ':' <<x<<endl
#define IN() freopen("usage.txt", "r", stdin)
#define OUT() freopen("charge.txt", "w", stdout)
using namespace std;

//global, checkpoint for limiter
int month, year, checkpoint = 0;

//calculate
void calc(int number[], double time[],int checkpoint){
	//data array
	int charge[checkpoint];
	int total[checkpoint];
	int average[checkpoint];
	OUT();
	cout << "Tagihan untuk " << month<<"/"<<year<< endl<< endl;
	cout << setw(15) << "Nopel" << setw(15) << "Jam Pakai" << setw(15) << "Biaya/jam" << setw(15) << "Total Biaya" << setw(15) << "Average" << endl;
	REP(i,checkpoint){
		//if time below 10hrs
		if(time[i] <= 10){
			charge[i] = 130000;
			total[i] = charge[i] * time[i];
			average[i] = ceil(charge[i]/time[i]);
		}
		else{
			//if time above 10hrs
			double temp = time[i] - 10;
			charge[i] = 130000 + (temp*15000);
			total[i] = 1300000 + (temp*15000);
			average[i] = ceil(charge[i]/time[i]);
		}

		//out
		cout << setw(15) << number[i] 	<<	setw(15) << time[i] <<	setw(15) << charge[i] 	<<	setw(15) << total[i] 	<<	setw(15) << average[i] 	<< endl;
	}
}

int main()
{
	ios_base::sync_with_stdio(false); cin.tie(0); IN();	//input file

	//max input length;	array data
	int length = 1000;	int number[length];	double time[length];

	//date input
	cin >> month >> year;

	//data input
	REP(i,length){
		cin >> number[i] >> time[i];	//stdin
		checkpoint++;	//limiter
		
		//eof;
		if(cin.get() == EOF){
			break;
		}
	}
	fclose(stdin);
	calc(number, time, checkpoint);
}
