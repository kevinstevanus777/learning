#include <bits/stdc++.h>
#define REP(i,n) for (int i = 0; i < (n); ++i)
using namespace std;

int main()
{	
	ios_base::sync_with_stdio(false); //cin.tie(0);
	int total = 100;	//length
	char input[total] = {'\0'};	char output[total] = {'\0'};	//declare
	cout << "Input = ";
	cin.getline(input,sizeof(input));	//stdin
	int temp = 0;	//dummy++
	
	//thinking
	REP(i,total){
		if(isupper(input[i]) && i == 0){
			output[temp] = input[i];
			temp++;
		}			
		else if(islower(input[i])){
			output[temp] = input[i];
			temp++;
		}
		else if(isupper(input[i])){
			output[temp] = ' ';			temp++;
			output[temp] = input[i];	temp++;
		}
	}

	//out
	cout << "Output = ";
	REP(i,total){
		cout << output[i];
	}
}
