	#include<iostream>		//i.o.
	#include<time.h>		//time
	#include<cstdlib>		//srand
	#include<string>
	#include<fstream>
	#include "MergeSort.cpp"
	#include "BubbleSort.cpp"
	#include "ExchangeSort.cpp"
	#include "InsertionSort.cpp"
	#include "QuickSort.cpp"
	#include "SelectionSort.cpp"
	#include "ShellSort.cpp"

using namespace std;
void reset(int data[], int temp[], int n);								//declare
void ztoa(int data[], int temp[],int n);
typedef void (*SpecialSnowFlake) (int array[], int sizeofarray);		//OneTypeDefFunction
typedef void (*AnotherSnowFlake) (int array[], int low, int high);

int main(){
	//what does this thing do ?
	cout << "Nama = Kevin Stevanus\nNIM = 825170005\n\n";
	cout << "if it went slower than average light time,DON'T PANIC!\n";

	//specialsecretsnowfloakeforme..		[just for fun]
	SpecialSnowFlake functions[] = 
	{
		bubbleSort,
		insertionSort,
		selectionsort,
		exch,
		shellSort
		//merge and quicksort are sold separately!
	};

	AnotherSnowFlake another[]=
	{
		mergeSort,
		quickSort
	};


	srand(time(NULL));		//so random.. what is this ??
	clock_t tick;			//ticktocks
	ofstream myfile;
	myfile.open ("outputexcel.xls");





//VARIABLE DECLARATION
	//declaring var 
	int n10000 = 10000, n20000 = 2*n10000, n40000 = 2*n20000, n80000 = 2*n40000;	//sizeofarray
	
	//array & temparray
	int data10k[n10000], data20k[n20000], data40k[n40000], data80k[n80000];			//array of data
	int temp10k[n10000], temp20k[n20000], temp40k[n40000], temp80k[n80000];			//array of temp (values are cloned from array data)
	
	//for loop
	int nmix[4] = {n10000,n20000,n40000,n80000};									//arrayof nmix
	int *tempmix[4] = {temp10k,temp20k,temp40k,temp80k};							//arrayof arraytemp
	int *datamix[4] = {data10k,data20k,data40k,data80k};							//arrayof arraydata

	string unused[3] = {"SORT","ASCENDING","DESCENDING"};
	string name[7] = {"BUB","INS","SEL","EXCH","SHELL","MERGE","QUICK"};		//func out
	string dummy[4] = {"10k","20k","40k","80k"};									//stringout
	
	//stored data
	// 3 records = sorting , ascending, descending
	double threerecord[3][4][7]; //interesting... [i = three records;j = size of data; k = 7 kind of sortings ]



//oh boi , it's 4 am again ?

//ASSIGNING VALUE
	
	//randomizing
	//10k data
	for(int i  = 0; i < n10000; i++){	
		temp10k[i] = data10k[i] = rand()%(2*n10000-1);	//assigning random value
	}	
	//20k data
	for(int i = 0; i < n20000; i++){
		temp20k[i] = data20k[i] = rand()%(2*n20000-1);
	}
	//40k data
	for(int i = 0; i < n40000; i++){
		temp40k[i] = data40k[i] = rand()%(2*n40000-1);	
	}
	//80k data
	for(int i = 0; i < n80000; i++){
		temp80k[i] = data80k[i] = rand()%(2*n80000-1);
	} 

	/*HOW TO USE THIS CODE???
	for(int i = 0; i < 4;i++){
		for(int j = 0; j< nmix[i];j++){
			(*tempmix+i)[j] = (*datamix+i)[j] = rand()%(2*nmix[i]-1);
		}
	}
	*/

//my lovely special snowflake
	//specialcode .. wait .. what is this ?!!!
	for(int i = 0 ; i < 4; i ++){
		int j = 0; //YOU SHALL PASS!
		for(j= 0; j<5;j++){
			for(int k = 0; k<3;k++){
				if(k == 2){
					ztoa((*(datamix+i)),(*(tempmix+i)),nmix[i]);
				}
				tick = clock();
				functions[j](*(tempmix+i), nmix[i]);
				tick = clock() - tick;	
				threerecord[k][i][j] = tick;
				cout << k<<","<<i<<","<<j<<", = "<<((float)threerecord[k][i][j])/CLOCKS_PER_SEC << " ";
				if(k == 2){
					reset((*(datamix+i)),(*(tempmix+i)),nmix[i]);
				}
			}
			cout << endl;
		}

		for(j = 5; j<7;j++){
			
			for(int k =0; k<3;k++){
				if(k == 2){
					ztoa((*(datamix+i)),(*(tempmix+i)),nmix[i]);					
				}
				tick = clock();
				another[j-5](*(tempmix+i),0,nmix[i]-1);
				tick = clock() - tick;
				threerecord[k][i][j] = tick;
				cout << k<<","<<i<<","<<j<<", = "<<((float)threerecord[k][i][j])/CLOCKS_PER_SEC << " ";				
				if(k == 2){
					reset((*(datamix+i)),(*(tempmix+i)),nmix[i]);				
				}
			}
			cout << endl;
		}
		cout << endl;
	}

	//bro you didn't understand how the code above works ? same...

	cout << endl << endl;
	
	//outtocsv
	for(int i = 0 ;i< 3;i++){
		myfile << unused[i] << "\n";
		for(int x = 0; x<7;x++){
				myfile << name[x] << "	";
			}
			myfile << "\n";
		for(int j = 0;j<4;j++){
			for(int k = 0;k<7;k++){
				cout << ((float)threerecord[i][j][k])/CLOCKS_PER_SEC << " ";
				myfile << ((float)threerecord[i][j][k])/CLOCKS_PER_SEC << "	";
			}
			myfile << "\n";
			cout << endl;
		}
		myfile << "\n\n";
		cout << endl <<endl;
	}

	myfile.close();

	//this output are for potato linux user like me..
	myfile.open("outputcsv.csv");

	for(int i = 0 ;i< 3;i++){
		myfile << unused[i] << "\n";
		for(int x = 0; x<7;x++){
				myfile << name[x] << ",";	//change this one too if it doesnt work
			}
			myfile << "\n";
		for(int j = 0;j<4;j++){
			for(int k = 0;k<7;k++){
				myfile << ((float)threerecord[i][j][k])/CLOCKS_PER_SEC << ","; //this is for csv ok ?!
				//README! if your csv doest work.. change the "," to ";"  ^^^<--that one.
			}
			myfile << "\n";
			cout << endl;
		}
		myfile << "\n\n";
		cout << endl <<endl;
	}	

	cout << "open the output file\nif it says corrupted, just open anyways. or rather.. try to use the csv instead\n";


}

//why am i left here man.. 
void reset(int data[], int temp[], int n){
	for(int i = 0; i < n; i++){
		temp[i] = data[i];
	}
}


void ztoa(int data[], int temp[],int n){
	int x=n-1;
	for(int i = 0; i < n; i++){
		temp[i] = data[x];
		x--;
	}
}
