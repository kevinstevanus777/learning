//questions
/*--------------------------------------------------------------------------------------------------------------------------*/
	/*	SOAL
	1.	Buatlah program utama untuk bubble sort, exchange sort, insertion sort, 
		selection sort, quick sort, merge sort, dan shell sort di atas lalu jalankan progam anda.
	2.	Tambahkan dalam program saudara fungsi untuk membangkitkan bilangan acak integer. 
		Buatlah array untuk menyimpan 10000, 20000, 40000 dan 80000 data. 
		Untuk membangkiktkan bilangan acak dapat digunakan statement berikut:
		srand(time(NULL));
		hsl_rand=rand()%(2*n-1); // n = banyaknya data
		Catat waktu yang dibutuhkan untuk mengurutkan data di atas. (catat clock sebelum dan sesudah proses pengurutan data) 
	3.	Bandingkan waktu yang dibutuhkan untuk mengurutkan data acak, 
		data urut dan data urut kebalikan untuk ketiga program tadi dan jelaskan. 
	*/
	//no1.. done
	//no2.. done
	//no3
	//tocsv
/*--------------------------------------------------------------------------------------------------------------------------*/

//libs
/*--------------------------------------------------------------------------------------------------------------------------*/
	#include<iostream>		//i.o.
	#include<time.h>		//time
	#include<cstdlib>		//srand
	#include<string>
	#include<fstream>
	#include "MergeSort.cpp"
	#include "BubbleSort.cpp"
	#include "ExchangeSort.cpp"
	#include "InsertionSort.cpp"
	#include "QuickSort.cpp"
	#include "SelectionSort.cpp"
	#include "ShellSort.cpp"
/*--------------------------------------------------------------------------------------------------------------------------*/

using namespace std;
void reset(int data[], int temp[], int n);								//declare
void ztoa(int data[], int temp[],int n);
typedef void (*SpecialSnowFlake) (int array[], int sizeofarray);		//OneTypeDefFunction
typedef void (*AnotherSnowFlake) (int array[], int low, int high);

int main(){
	//what does this thing do ?
	cout << "Nama = Kevin Stevanus\nNIM = 825170005\n\n";

//this is just the beginning!	
	//specialsecretsnowfloakeforme..		[just for fun]
	SpecialSnowFlake functions[] = 
	{
		bubbleSort,
		insertionSort,
		selectionsort,
		exch,
		shellSort
		//merge and quicksort are sold separately!
	};

	AnotherSnowFlake another[]=
	{
		mergeSort,
		quickSort

	};


	srand(time(NULL));		//so random.. what is this ??
	clock_t tick;			//ticktocks
	ofstream myfile;
	myfile.open ("output.csv");





//VARIABLE DECLARATION
	//declaring var 
	int n10000 = 10000, n20000 = 2*n10000, n40000 = 2*n20000, n80000 = 2*n40000;	//sizeofarray
	//array & temparray
	int data10k[n10000], data20k[n20000], data40k[n40000], data80k[n80000];			//array of data
	int temp10k[n10000], temp20k[n20000], temp40k[n40000], temp80k[n80000];			//array of temp (values are cloned from array data)
	//for loop
	int nmix[4] = {n10000,n20000,n40000,n80000};									//arrayof nmix
	int *tempmix[4] = {temp10k,temp20k,temp40k,temp80k};							//arrayof arraytemp
	int *datamix[4] = {data10k,data20k,data40k,data80k};							//arrayof arraydata


	string name[7] = {"bubble","insertion","selection","exchange","shellsort","mergesort","quicksort"};		//func out
	string dummy[4] = {"10k","20k","40k","80k"};									//stringout
	//stored data
	double sortedrecord[4][7],ascendingrecord[4][7],descendingrecord[4][7];




//ASSIGNING VALUE
	//randomizing
	//10k data
	for(int i  = 0; i < n10000; i++){	
		data10k[i] = rand()%(2*n10000-1);	//assigning random value
		temp10k[i] = data10k[i];			//cloning
	}	
	//20k data
	for(int i = 0; i < n20000; i++){
		data20k[i] = rand()%(2*n20000-1);
		temp20k[i] = data20k[i];
	}
	//40k data
	for(int i = 0; i < n40000; i++){
		data40k[i] = rand()%(2*n40000-1);
		temp40k[i] = data40k[i];
	}
	//80k data
	for(int i = 0; i < n80000; i++){
		data80k[i] = rand()%(2*n80000-1);
		temp80k[i] = data80k[i];
	}

//my lovely special snowflake
	//special
	for(int i = 0 ; i < 4; i ++){
		int j = 0; //YOU CAN  PASS!
		for(j= 0; j<5;j++){



			// UNSORTED DATA TO SORTED DATA 
			tick = clock();																		//start timer
			functions[j](*(tempmix+i), nmix[i]);												//throw a function
			tick = clock() - tick;																//end timer
			sortedrecord[i][j] = tick;															//record the value
			cout << name[j] << dummy[i] << " = " << 	((float)tick)/CLOCKS_PER_SEC << endl;	//output

			// SORTED DATA TO SORTED DATA [ascending]
			tick = clock();
			functions[j](*(tempmix+i), nmix[i]);
			tick = clock() - tick;
			ascendingrecord[i][j] = tick;

			ztoa((*(datamix+i)),(*(tempmix+i)),nmix[i]);
			tick = clock();
			functions[j](*(tempmix+i), nmix[i]);
			tick = clock() - tick;
			descendingrecord[i][j] = tick;

			reset((*(datamix+i)),(*(tempmix+i)),nmix[i]);										//reset data



		}

		for(j = 5; j<7;j++){
			tick = clock();
			another[j-5](*(tempmix+i), 0, nmix[i]-1);
			tick = clock() - tick;
			sortedrecord[i][j] = tick;
			cout << name[j] << dummy[i] << " = " << 	((float)tick)/CLOCKS_PER_SEC << endl;	//output
			//asc

			tick = clock();
			another[j-5](*(tempmix+i), 0, nmix[i]-1); 
			tick = clock() - tick;
			ascendingrecord[i][j] = tick;	
				
			ztoa((*(datamix+i)),(*(tempmix+i)),nmix[i]);
			tick = clock();
			another[j-5](*(tempmix+i), 0, nmix[i]-1); 
			tick = clock() - tick;
			descendingrecord[i][j] = tick;



			reset((*(datamix+i)),(*(tempmix+i)),nmix[i]);										//reset data

		}

		cout << endl;
	}

	cout << endl << endl;
	
	//output sortedrecord	
	cout << "sortedrecord\n";
	myfile << "sortedrecord\n";
	for(int i = 0; i < 4; i++){
		for(int j = 0; j< 7;j++){
			cout << ((float)sortedrecord[i][j])/CLOCKS_PER_SEC << " ";
			myfile << ((float)sortedrecord[i][j])/CLOCKS_PER_SEC << ",";
		}
		cout << endl;
		myfile << "\n";
	}

	//output ascendingrecord	
	cout << "ascendingrecord\n";
	myfile << "\nascendingrecord\n";
	for(int i = 0; i < 4; i++){
		for(int j = 0; j< 7;j++){
			cout << ((float)ascendingrecord[i][j])/CLOCKS_PER_SEC << " ";
			myfile << ((float)ascendingrecord[i][j])/CLOCKS_PER_SEC << ",";
		}
		cout << endl;
		myfile << "\n";
	}	

	//output descendingrecord	
	cout << "descendingrecord\n";
	myfile << "\ndescendingrecord\n";
	for(int i = 0; i < 4; i++){
		for(int j = 0; j< 7;j++){
			cout << ((float)descendingrecord[i][j])/CLOCKS_PER_SEC << " ";
			myfile << ((float)descendingrecord[i][j])/CLOCKS_PER_SEC << ",";
		}
		cout << endl;
		myfile << "\n";
	}	


}

//why am i left here man.. 
void reset(int data[], int temp[], int n){
	for(int i = 0; i < n; i++){
		temp[i] = data[i];
	}
}


void ztoa(int data[], int temp[],int n){
	int x=n-1;
	for(int i = 0; i < n; i++){
		temp[i] = data[x];
		x--;
	}
}
