#include<iostream>
#include<cstdio>
#include<string>
#include<string.h>
#include <stdio.h>
#include <ctype.h>
#include<iomanip>
#include<cstring>		//this one used for char inside struct

//use struct, recursive,iterative, pointer of array, what else?



/* SOAL
: Sebuah perusahaan mempunyai 200 pegawai yang terbagi menjadi 3 bagian yaitu bagian administrasi, 
bagian penjualan dan bagian kebersihan. Perhitungan gajinya adalah sebagai berikut:
a. administrasi: gaji pokok + tunjangan kesehatan + uang transport.
b. penjualan: uang transport + komisi 5% dari total nilai penjualan.
c. kebersihan: gaji pokok + uang transport 
Uang transport dihitung berdasarkan jumlah hari masuk kerja.
*/



//declaring struct untuk 
//"a.	Buatlah sebuah struktur data struct untuk menyimpan semua data pegawai di atas. "
struct karyawan{
	bool administrasi, penjualan, kebersihan;
	char nama[20];
	int gaji, tunjangan, transport, komisi;
};


//you know what is this right?
using namespace std;



//fungsi rekursifnya 
										//yang diterima adalah alamatnya bukan valuenya!
rekursif(karyawan kr[], int sizeofarray, int &a){

	//deklarasi var yang dipakai sebagai dummy
	int input = 0;



	if(a == sizeofarray){
		cout << "DONE";
	}
	else{

		//input nama
		//disini rusak juga, hmm --
		cout << "Masukkan nama = ";
		cin.ignore( 1000, '\n' );
		cin.getline(kr[a].nama,sizeof(kr[a].nama));		// <- meminta input dari nama user contoh mudahnya adalah = cin.getline(input, sizeof(input)) 	
														// cuma dibikin ribet karena adaa getline, struct, pointer
		

		//toupper
		int b =0;
		char c;	//var dummy
		while(kr[a].nama[b]){
			c = kr[a].nama[b];			//c adalah satuan dari var 'kr[a].nama[b]'; maksudnya misal input KEVIN , yang baru di ambil cuma 'k'; karena 'kr[a].nama[b]' <- yang diambil baru b(masih satuan);
			c = toupper(kr[a].nama[b]);	//c di jadiin CAPS
			kr[a].nama[b] = c; 			//yang tadi di caps , dilempar lagi ke variabel utama 
			b++;						//increment ampe hurufnya abis
		}






		//input divisi
		cout 	<<	"Bagian ? [1.admin, 2. penjualan, 3. kebersihan] = ";
		cin  	>>	input;	// <- dilempar ke var input dulu(var temp )


		//analisis
		if(input == 1){
			kr[a].administrasi = true;		kr[a].penjualan = false;	kr[a].kebersihan = false;
		}
		else if(input == 2){
			kr[a].administrasi = false;		kr[a].penjualan = true;		kr[a].kebersihan = false;
		}
		else if(input == 3){
			kr[a].administrasi = false;		kr[a].penjualan = false;	kr[a].kebersihan = true;
		}



		cout << "gaji = ";
		cin >> kr[a].gaji;

		cout << "tunjangan = ";
		cin >> kr[a].tunjangan;

		cout << "transport = ";
		cin >> kr[a].transport;

		cout << "komisi = ";
		cin >> kr[a].komisi;

		//rekursifnya 
		rekursif(kr,sizeofarray,a = a+1);
	}
}



int main()
{	

	int sizeofarray = 0;


	//declare var menggunakan pointer dan reference
	//gunanya adalah untuk dummy var yaitu increment di rekursif
	int temp = 0;	//deklarasi var temp bernilai 0
	int *a;			//deklarasi var a tapi menggunakan POINTER
	a = &temp;		//A ADALAH ALAMAT DARI TEMP
	//setiap kali ada perbuahan dari variabel a, isi dari temp juga berubah . 
	//KARENA A ADALAH ALAMAT (BUKAN VALUE) DARI TEMP

	//buat orang yang 'kurang pintar' boleh dicek dibawah , kalo dah ngerti ya skip aja 
	/*
	a= &temp;
	(jadinya yang dibawah)

	a 			=				&			temp;
	var a   	adalah   		ALAMAT 		dari var temp

	*/


	//meminta input seberapa banyak jumlah karyawan
	cout << "berapa jumlah karyawan ? = ";
	cin >> sizeofarray;

	//declare struct
	karyawan kr[sizeofarray];


//rusaknya disini!
	//emptying the value
	
	for(int i = 0; i < sizeofarray; i++){
		kr[i].administrasi = false;
		kr[i].penjualan = false;
		kr[i].kebersihan = false;

		
		kr[i].gaji = 0;
		kr[i].tunjangan = 0;
		kr[i].transport = 0;
		kr[i].komisi = 0;



		//emptying the value of char nama dari struct karyawan
		for(int j = 0 ; j < sizeof(kr[j].nama);j++){
			kr[i].nama[j] = '\0';	// <- lihat! ini lebih susah dikit. 
		}
	} 
	//end of emptying the value




	//lempar ke void rekursif
	rekursif(kr, sizeofarray, *a); 
	//YANG DIKIRIM HARUS *a , artinya yang dikirim adalah valuenya, jangan alamatnya



	//iteratif
	for(int i = 0; i < sizeofarray; i++){
		
		if(kr[i].administrasi == true){
			cout << kr[i].nama << endl;
		}
		
	}



	return 0;
}
