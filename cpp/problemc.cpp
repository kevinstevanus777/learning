//ICPC2017_PROBLEM_C_SOLVING

//including libraries
#include<iostream>	//standard i/o
#include<stdio.h>	//used to use 'gets'
#include<ctype.h>	//used to use 'isspace'


using namespace std;


int main(){


//declaring var
int row = 0;
int column = 0;
int x = 0;

//input row and column
cin >> row >> column;



int frontview[row];
int sideview[column];
int topview[row][column];
int data[row][column];
int donttouch[row][column];
int colcount[column];


//emptying value
for(int i = 0;i < row;i++){
	for(int j = 0; j<column;j++ ){
		data[i][j] = 0;
		topview[i][j] = 0;
		donttouch[i][j] = 0;
	}
	frontview[i] = 0;
	sideview[i] = 0; 
	colcount[i] = 0;
}



//input data
for(int i = 0;i < row;i++){
	for(int j = 0; j<column;j++ ){
		cout << "j[" <<i<<"]["<<j<<"] = ";
		cin >> data[i][j]; 
	}
}




//find value! [find max]
for(int i = 0;i < row;i++){
	for(int j = 0; j<column;j++ ){
		//findmax topview
		if(data[i][j] > 0){
			topview[i][j] = 1;
		}

		//find maxsideview
		if(data[i][j] > sideview[i]){
			sideview[i] = data[i][j];
		}
		//find maxfrontview;
		if(data[j][i] > frontview[i]){
			frontview[i] = data[j][i];
		}
	}
}

//donttouch [this one used to find value of the highest crates that shouldn't be touched]
for(int i = 0;i < row;i++){
	for(int j = 0; j<column;j++ ){
		


		//find maxsideview
		if(data[i][j] == sideview[i] && topview[i][j] != 0){
			donttouch[i][j] = 1;
			colcount[i]++;

		}
		//find maxfrontview;
		if(data[j][i] == frontview[i] && topview[i][j] != 0){
			donttouch[i][j] = 1;

		}
	}
}


cout  << endl << "old things || height that shouldn't be touched"<<endl;
for(int i = 0 ; i < row; i ++){
	for(int j = 0; j< column; j++){
		cout << donttouch[i][j] << " ";
	}
	cout << endl;
}




//view output [giving output of maxview for each cameras]
cout << "\ntopview\n";
for(int i = 0;i < row;i++){
	for(int j = 0; j<column;j++ ){
		cout << topview[i][j] << " ";
	}
	cout << endl;
}
cout << endl;
cout << "testcase cout sideview\n";
for(int i =0;i<column;i++){
	cout << sideview[i] << " ";
}
cout << endl;

cout << "testcase cout frontview\n";
for(int i =0;i<column;i++){
	cout << frontview[i] << " ";
}




//newmade
cout << endl << "NEWMADE || height that shouldn't be touched" << endl;
for(int i = 0 ; i < row; i ++){
	for(int j = 0; j< column; j ++){
		if(donttouch[i][j] == 1 && data[i][j] == sideview[i] && data[i][j] < frontview[j]){
			cout << "colcount = " << colcount[i] << endl;
			if(colcount[i] == 1){
				colcount[i] = 0;
			}
			else{

			donttouch[i][j] = 0;
			colcount[i]--;	
			}
		}
	}
}


cout  << endl << endl;
for(int i = 0 ; i < row; i ++){
	for(int j = 0; j< column; j++){
		cout << donttouch[i][j] << " ";
	}
	cout << endl;
}


//find x!
int old = 0;
while(true){
	for(int i = 0 ; i< row; i++){
		for(int j = 0; j< column;j++){
			if(donttouch[i][j] != 1 && topview[i][j] > 0 && data[i][j] > 1){
				data[i][j]--;
				x++;
			}
		}
	}

	if(old == x){
		break;
	}

	old = x;

}
cout <<  "is this good? x = " << x << endl;


//cout << "isthis good? = " << x << endl;


return 0;

}






/*
//saved for later
int row;
int column;
//input variable
char input[10];
//emptying the value
for(int i = 0;i<10;i++){
	input[i] = '\0';
}
//getting input
gets(input);
cout << endl;
for(int i = 0;i<10;i++){
	if(isspace(i)){
		cout << "lol";
	}
}*/
