//no3
#include <bits/stdc++.h>
#define IN() freopen("DATA_MHS.txt", "r", stdin)
#define REP(i,n) for (int i = 0; i < (n); ++i)
using namespace std;

/*struct data mahasiswa*/
struct data{
	int nim;			//nomor induk
	char nama[20];		//nama lengkap
	int pstudi;			//program studi
	double ipkbefore;	//ipk sebelumnya
	int sems;			//semester yang sudah ditempuh
};

int main(){
	IN();//infile
	ios_base::sync_with_stdio(false);

	int sizeofarray = 1000;	//besar array

	//dms == data mahasiswa
	data *dms;

	//initiate new array size
	dms = new data[sizeofarray];
	int increment = 0;

	cout << "Input jumlah mahasiswa = ";
	int jml;
	cin >> jml;

	while(increment != jml){
		//input nim
		cout << "Input NIM = ";
		cin >> (*(dms+increment)).nim;

		//input nama
		cout << "Input Nama Lengkap = ";
		cin.ignore( 1000, '\n' );
		cin.getline(dms[increment].nama,sizeof(dms[increment].nama));

		//program studi
		cout << "Input Program Studi = ";
		cin >> (*(dms+increment)).pstudi;

		if((*(dms+increment)).pstudi > 3 || (*(dms+increment)).pstudi < 1){	//limiter bila input pstudi outofrange, menjadi default yaitu 3 
			(*(dms+increment)).pstudi = 3;
		}

		//ipk
		cout << "Input IPK = ";
		cin >> (*(dms+increment)).ipkbefore;

		//semester
		cout << "Input Semester yang sudah ditempuh = ";
		cin >> (*(dms+increment)).sems;

		//i++
		increment++;
	}
	cout << endl;

	system("cls");	//clear terminal , for windows

	//title
	cout << setw(20) << "Nama" <<
			setw(10) << "NIM" <<
			setw(20) << "Program Studi" <<
			setw(5) << "IPK" <<
			setw(5) << "Sems" << endl;

	//data output
	REP(i,jml){
		if((*(dms+i)).sems < 5 && (*(dms+i)).ipkbefore > 3.50 || (*(dms+i)).sems >= 5 && (*(dms+i)).ipkbefore > 3.00){	//syarat output dr soal
			cout << setw(20) << dms[i].nama;		//nama lengkap
			cout << setw(10) << (*(dms+i)).nim;		//nim
			
			//output program studi
			if((*(dms+i)).pstudi == 1){
				cout << setw(20) << "Teknik Informatika";
			}
			else if((*(dms+i)).pstudi == 2){
				cout << setw(20) << "Sistem Informasi";
			}			
			if((*(dms+i)).pstudi == 3){
				cout << setw(20) << "Program Studi Lain";
			}

			cout << setw(5) << (*(dms+i)).ipkbefore;//ipk terakhir
			cout << setw(5) << (*(dms+i)).sems;		//semester
			cout << endl;			
		}
	}
}
