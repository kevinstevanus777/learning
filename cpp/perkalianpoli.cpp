#include <bits/stdc++.h>
#include <bits/stdc++.h>

using namespace std;


struct Node
  {
  	int angka;
  	int pangkat;
    Node *next;
  };	
  
void InsertNode(Node **L, int angka, int pangkat){
	Node *baru;
	baru = new Node;
	baru -> angka = angka;
	baru -> pangkat = pangkat;
	baru -> next = NULL;
	if(*L == NULL){
		*L = baru;
	}

	else{
		Node *t;
		t = *L;
		while(t-> next	!= NULL){
			 t= t-> next;
		}
		t-> next = baru;
	}

}


void PrintList(Node *L){
	Node *n;
	n = L;
	while(n-> next != NULL){
		cout << n-> angka << "x" << n-> pangkat<< " + ";
		n = n-> next;
	}

	if(n-> next == NULL){
		cout << n-> angka<< "x" << n-> pangkat << endl;
	}
}

void perkalian(Node *P1, Node *P2){
	Node *a, *b;
	a = P1;
	b =  P2;


	Node *hasil;
	hasil = NULL;
	

	while(a != NULL && b!= NULL){
		

		int tempangka = a->angka * b->angka; //var tempangka berisi hasil dari perkalian node a dan b 
		int temppangkat = a->pangkat + b->pangkat;	//var pangkat berisi hasil dari tambah node a dan b 

		InsertNode(&hasil, tempangka, temppangkat);	//lempar ke fungsi

		b =  b->next;		//b++

		if(b == NULL){
			a = a->next;
			b = P2;
		}
		
	}

	PrintList(hasil);


}



int main(){
	ios_base::sync_with_stdio(false); /*cin.tie(0);*/
	
	Node *P1, *P2;
	P1 = P2 = NULL;

	for(int i =1;i<3;i++){
		int n;
		cout << "ketik banyak suku dalam poli " << i << " = ";
		cin >> n ;
		cout << endl;

		for(int j=1; j <= n;j++){
			int angka, pangkat;
			cout << "Masukkan konstanta untuk suku ke " << j << " = ";
			cin >> angka;
			cout << "Masukkan pangkat untuk suku ke " << j << " = ";
			cin >> pangkat;

			if(i == 1){
				InsertNode(&P1, angka, pangkat);
			}
			else{
				InsertNode(&P2, angka, pangkat);
			}
			cout << endl;
		}
		cout << endl;
	}
	
	cout << "Persamaan 1" << endl;
	PrintList(P1);

	cout << "Persamaan 2" << endl;
	PrintList(P2);

	cout << "Hasil perkalian = "<< endl;
	perkalian(P1, P2);
	

}
