//no2
#include <bits/stdc++.h>
#define REP(i,n) for (int i = 0; i < (n); ++i)
using namespace std;

char* rev(char arr[], int length){

	bool ding = false;
	char vocal[5] = {'A','I','U','E','O'};
	char boi[3] = {'S','A','Y'};
	REP(i,5){
		if(arr[0] == vocal[i]){
			ding = true;
		}
	}

	REP(i,3){
		if(i == 0 && ding == false){
			arr[length+i] = arr[0];
			continue;
		}
		arr[length+i] = boi[i];
	}

	return arr;
}


int main()
{

	//declare
	char input[50] = "\0";

	//input
	cout << "Input = ";
	cin.getline(input,sizeof(input));

	//length
	int length = strlen(input);

	//toupper
	REP(i,length){
		input[i] = toupper(input[i]);
	}

	//lempar void
	char *output = rev(input,length);

	//out
	cout << "Output = " << output <<endl;
}
