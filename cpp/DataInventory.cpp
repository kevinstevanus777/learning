#include<iostream>
#include<iomanip>

using namespace std;

//declare struct awal
struct inventory
{
	int nomorproduk;		//untuk nomor produknya(dibuat secara otomatis per data stok)
	char namaproduk[20];	//untuk nama produk
	char namasupplier[20];	//untuk nama supplier
	int hargaperunit;		//untuk harga barang
	int jumlahunit;			//untuk jumlah unit yang dimiliki
	bool low;				//bila barang dibawah 100, low == true
	bool high;				//bila barang diatas 1000 , high == true 
	//char keterangan[20];	//keterangan untuk barangnya

};


//deklarasi fungsi void
void baru(inventory inv[], int &n);
void check(inventory inv[], int &n);
void update(inventory inv[], int &n);



//fungsi untuk menerima data baru dari user
void baru(inventory inv[], int &n){

	system("cls");

	//input nomor produk [LAMA]
	//cout << "Input nomor produk = ";
	//cin >> inv[n].nomorproduk;

	//deklarasi nomor produk secara otomatis
	inv[n].nomorproduk = n+1;

	//input nama produk
	cout << "Input nama produk = ";
	cin >> inv[n].namaproduk;

	//input nama supplier
	cout << "Input nama supplier = ";
	cin >> inv[n].namasupplier;

	//input harga per unit;
	cout << "Input harga per unit = ";
	cin >> inv[n].hargaperunit;

	//input jumlah unit;
	cout << "Input jumlah unit = ";
	cin >> inv[n].jumlahunit;

	//jika unit dibawah 100 , bool low = true
	if(inv[n].jumlahunit <= 100){
		inv[n].low = true;
		inv[n].high = false;
	}

	//jika unit diatas 1000, bool high = true
	else if(inv[n].jumlahunit > 1000){
		inv[n].high = true;
		inv[n].low = false;
	}

	//jika x>100 && x<1000, high & low = false
	else{
		inv[n].low = false;
		inv[n].high = false;
	}

	//menambah 1 elemen baru di jumlah array untuk input data baru selanjutnya
	n++;
}

void check(inventory inv[], int &n){


	system("cls");
	
	cout << "High Inventory\n";	
	//header
	cout << setw(20) << "Nomor Produk";
	cout << setw(20) << "Nama Produk";
	cout << setw(20) << "Nama Supplier";
	cout << setw(20) << "Harga PerUnit";
	cout << setw(20) << "Jumlah Unit";
	cout << setw(20) << "Keterangan\n"; 

	//high inventory
	for(int i =0; i < n ; i++){
		if(inv[i].high == true){
			cout << setw(20) << inv[i].nomorproduk;
			cout << setw(20) << inv[i].namaproduk;
			cout << setw(20) << inv[i].namasupplier;
			cout << setw(20) << inv[i].hargaperunit;
			cout << setw(20) << inv[i].jumlahunit;
			cout << setw(20) << "High Inventory";
			cout << endl;
		}
	}

	cout << endl;	//haloo spasi
	
	cout << "Low Inventory\n";
	//header
	cout << setw(20) << "Nomor Produk";
	cout << setw(20) << "Nama Produk";
	cout << setw(20) << "Nama Supplier";
	cout << setw(20) << "Harga PerUnit";
	cout << setw(20) << "Jumlah Unit";
	cout << setw(20) << "Keterangan\n"; 

	//low inventory
	for(int i =0; i < n ; i++){
		if(inv[i].low == true){
			cout << setw(20) << inv[i].nomorproduk;
			cout << setw(20) << inv[i].namaproduk;
			cout << setw(20) << inv[i].namasupplier;
			cout << setw(20) << inv[i].hargaperunit;
			cout << setw(20) << inv[i].jumlahunit;
			cout << setw(20) << "Low Inventory";
			cout << endl;
		}
	}

	cout << endl;

	cout << "Tidak Lebih Tidak Kurang\n";
	//header
	cout << setw(20) << "Nomor Produk";
	cout << setw(20) << "Nama Produk";
	cout << setw(20) << "Nama Supplier";
	cout << setw(20) << "Harga PerUnit";
	cout << setw(20) << "Jumlah Unit";
	cout << setw(20) << "Keterangan\n"; 

	//low inventory
	for(int i =0; i < n ; i++){
		if(inv[i].low == false && inv[i].high == false){
			cout << setw(20) << inv[i].nomorproduk;
			cout << setw(20) << inv[i].namaproduk;
			cout << setw(20) << inv[i].namasupplier;
			cout << setw(20) << inv[i].hargaperunit;
			cout << setw(20) << inv[i].jumlahunit;
			cout << setw(20) << "None";
			cout << endl;
		}
	}
}

void update(inventory inv[], int &n){

	system("cls");
	

	//digunakan untuk input user
	int temp = 0;

	//digunakan untuk input ditambah atau dikurang
	int dummy =0;

	//jinput user untuk mencari nomor produk yang ingin diubah unit stoknya
	cout << "Masukkan nomor produk yang ingin di update stocknya\n";
	cout << "Nomorproduk = ";
	cin >> temp;

	//input user untuk jumlah yang akan ditambah atau dikurangkan
	cout << "Masukkan jumlah yang ingin ditambah atau dikurang (pengurangan pakai simbol minus)" << endl;
	cout << "Jumlah = ";
	cin >> dummy;

	//for-nya digunakan untuk looping increment
	for(int i = 0; i < n; i++){
		//ifnya adalah.. kalau isi var nomor produk setara dengan var temp
		if(inv[i].nomorproduk == temp){
			//var jumlah unit akan bertambah 
			inv[i].jumlahunit += dummy;

			//updating boolean
			//jika unit dibawah 100 , bool low = true
			if(inv[i].jumlahunit <= 100){
				inv[i].low = true;
				inv[i].high = false;
			}

			//jika unit diatas 1000, bool high = true
			else if(inv[i].jumlahunit > 1000){
				inv[i].high = true;
				inv[i].low = false;
			}

			//jika x>100 && x<1000, high & low = false
			else{
				inv[i].low = false;
				inv[i].high = false;
			}
		}
	}
}

int main(){


	//kontrol input dari user
	int input = 0;

	//deklarasi besar array dinamis dengan pointer

	//!!!!!kalo ada masalah, coba mainin di sizeofarray ini  (jadiin 0/1)
	int sizeofarray = 200;		//deklarasi awal(sizeofarray)
	

	int dummy = 0;		//untuk di increment, setiap kali ada data baru 
	int *n;				//deklarasi pointer n
	n = &dummy;			//n adalah referensi alamat memori dari sizeofarray

	//inventory *ptr,inv;
	//ptr = &inv;


	//declare struct
	inventory inv[sizeofarray];



	//zeroing the value
	for(int i = 0;i < sizeofarray;i++){
		
		// bool low&high jadi false
		inv[i].low = 0;
		inv[i].high = 0;

		//zero nomor produk
		inv[i].nomorproduk = '\0';

		//zero hargaperunit
		inv[i].hargaperunit = '\0';

		//zero jumlah unit
		inv[i].jumlahunit = '\0';

		//zero namaproduk
		for(int j = 0; j <20;j++){
			inv[i].namaproduk[j] = '\0';
		}

		//zero nama supplier
		for(int j = 0; j <20;j++){
			inv[i].namasupplier[j] = '\0';
		}

		//zero keterangan
		//for(int j = 0; j <20;j++){
		//	inv[i].keterangan[j] = '\0';
		//}

	}




	do{


		//meminta input user
		cout << "\nProgram data inventory" << endl;
		cout << "1. Input Data Stock baru\n";
		cout << "2. Check Unit Data Stock\n";
		cout << "3. Update Data Stock\n";
		cout << "4. Exit\n";
		cout << "Input = ";
		cin >> input;


		//control untuk penentuan fungsi mana yang akan dipakai
		if(input == 1){
			baru(inv, *n);
		}
		else if(input == 2){
			check(inv, *n);
		}	
		else if(input == 3){
			update(inv,*n);
		}
		else if(input != 1 && input != 2 && input != 3){
			cout << "Input Anda salah!\n";
		}

	}
	while(input != 4);


	cout << "Bye!\n";
}
