#include<iostream>
#include<string.h>
#include<iomanip>

using namespace std;

struct iz{
	bool broker;
	bool salesperson;
};

struct kel{
	bool pria;
	bool wanita;
};

struct survey{
	iz izin;			//izin broker || salesperson
	int kota;			//kota 
	int umur;			//umur 
	kel kelamin;		//l/p
	int pendidikan;		//1/2/3/4
};

//declare fungsi
void kota();
void reset(survey sur[], int &n, int sizeofarray);
void baru(survey sur[], int &n);
void analisis(survey sur[], int &n);
void report(survey sur[], int &n);



void baru(survey sur[], int &n){
	

	//ini gaperlu jadi char karena di soal cuma diminta kodenya aja 
	//soal: Input data dari kuesioner berisi 5 jawaban: kode jenis izin, kode kota, umur, kode jenis kelamin, dan kode pendidikan. 
	int izin, kota, umur, kelamin, pendidikan;


	do{
		cout << "Input 0 bila ingin mengakhiri LOOP!\n\n";

//JENIS IZIN
/*------------------------------------------------------------------------*/		
		//input jenis izin 
		cout << "Jenis Izin [1. Broker/ 2. Salesperson]= ";
		cin >> izin;
		
		//menentukan boolean dari izin broker
		if(izin == 1){
			sur[n].izin.broker = true;
			sur[n].izin.salesperson = false;
		}
		else if(izin == 2){
			sur[n].izin.broker = false;
			sur[n].izin.salesperson = true;
		}	
/*------------------------------------------------------------------------*/



//KOTA
/*------------------------------------------------------------------------*/		
		//input kode kota
		cout << "Kode Kota = ";
		cin >> kota;

		//lempar isi var kota ke struct 
		sur[n].kota = kota-1;
/*------------------------------------------------------------------------*/		


//UMUR
/*------------------------------------------------------------------------*/
		//input umur
		cout << "Umur = ";
		cin >> umur;

		//lempar isi var umur ke struct
		sur[n].umur = umur;
/*------------------------------------------------------------------------*/



//KELAMIN
/*------------------------------------------------------------------------*/		
		//input jenis kelamin
		cout << "Sex [1. Laki-Laki; 2. Perempuan] =  ";
		cin >> kelamin;

		//boolean untuk jenis kelamin
		if(kelamin == 1){
			sur[n].kelamin.pria = true;
			sur[n].kelamin.wanita = false;
		}
		else if(kelamin == 2){
			sur[n].kelamin.pria = false;
			sur[n].kelamin.wanita = true;
		}

		//zero value
		kelamin == '\0';
/*------------------------------------------------------------------------*/


//PENDIDIKAN
/*------------------------------------------------------------------------*/	 	
	 	//input jenis pendidikan
	 	cout << "Pendidikan [ 1. Tidak Lulus, 2. SMU, 3. S1, 4. S2\n";
	 	cout << "Input = ";
	 	cin >> pendidikan;

	 	//lempar ke struct
	 	sur[n].pendidikan = pendidikan;
/*------------------------------------------------------------------------*/



		//menambah 1 elemen baru di jumlah array untuk input data baru selanjutnya
	 	n++;

	 	system("cls");

	}
	//eof method
	while(izin != 0 && kota != 0 && umur != 0 && kelamin != 0 && pendidikan != 0);
	
}



void analisis(survey sur[], int &n){

system("cls");
//RATA UMUR
/*------------------------------------------------------------------------*/
	//declare awal 
	int rataumur = 0;

	//menghitung umur
	for(int i = 0 ; i < n ; i++){
		rataumur += sur[i].umur;
	}

	//membuat rata2 umur
	rataumur = rataumur / (n-1);

	cout << "Rata-rata umur responden = " << rataumur << endl;
/*------------------------------------------------------------------------*/




//JUMLAH RESPONDEN DARI TIAP KOTA
/*------------------------------------------------------------------------*/

	int array = 100;

	string namakota[100] ={"Banda Aceh","Langsa","Meulaboh","Denpasar","Tangerang",
							"Bengkulu","Serang","Gorontalo","Jambi","Bandung",
							"Bekasi","Bogor","Cimahi","Cirebon","Depok",
							"Sukabumi","Tasikmalaya","Banjar","Magelang","Pekalongan",
							"Purwokerto","Salatiga","Semarang","Surakarta","Tegal",
							"Batu Alur","Blitar","Kediri","Madiun","Malang",
							"Mojokerto","Pasuruan","Probolinggo","Surabaya","Pontianak",
							"Singkawang","Banjarbaru","Banjarmasin","Palangkaraya","Balikpanpan",
							"Bontang","Jayapura","Makassar","Palu","Manado",
							"Kendari","Bitung","Padang","Dumai","Yogyakarta"}; 

	int kota[array] ={'\0'};
	int dummy[array];


	//mengisi angka dr 1-100
	for(int i = 0;i<array; i++){
		
		//isi dr 1-100
		dummy[i] = i;
		//zeroing value
		kota[i] = '\0';

	}

	//scanning
	for(int i = 0;i < n;i++){
		for(int j = 0;j<array;j++){
			if(sur[i].kota == dummy[j]){
				kota[j]++;
			}
		}
	}



	for(int i = 0;i<100;i++){
		if(kota[i] > 0){
			cout << namakota[i] << " = " << kota[i] << endl;
		}
	}



/*------------------------------------------------------------------------*/







//PERSENTASE COWO DAN CEWE
/*------------------------------------------------------------------------*/
	//declare int 
	float cowo = 0;
	float cewe = 0;


	//scanning data
	for(int i = 0; i< n;i++){
		if(sur[i].kelamin.pria == true && sur[i].kelamin.wanita == false ){
			cowo++;
		}
		
		else if(sur[i].kelamin.wanita == true && sur[i].kelamin.pria == false){
			cewe++;
		}		
	}


	//cari persen
	cowo = (cowo / (n-1)) * 100;
	cewe = (cewe / (n-1)) * 100;

	//output
	cout << "Persentasi Pria   = " << cowo << "%\n";
	cout << "Persentasi Wanita = " << cewe << "%\n";
/*------------------------------------------------------------------------*/	





//KEPINTARAN DARI RESPONDEN
/*------------------------------------------------------------------------*/


	//declaring
	int class1 = 0, class2 = 0, class3 = 0, class4 = 0;

	//scanning
	for(int i = 0; i < n; i++){
		if(sur[i].pendidikan == 1){
			class1++;
		}
		else if(sur[i].pendidikan == 2){
			class2++;
		}
		else if(sur[i].pendidikan == 3){
			class3++;
		}
		else if(sur[i].pendidikan == 4){
			class4++;
		}
	}


	cout << "Jumlah Tidak Lulus = " << class1 << endl;
	cout << "Jumlah Tamat SMA   = " << class2 << endl;
	cout << "Jumlah S1          = " << class3 << endl;
	cout << "Jumlah S2          = " << class4 << endl;






/*------------------------------------------------------------------------*/

}

void kota(){
	cout 	<< 	"1. Banda Aceh;2. Langsa;3. Meulaboh;4. Denpasar;5. Tangerang;\n"
			<<	"6. Bengkulu;7. Serang;8. Gorontalo;9. Jambi;10. Bandung\n"
			<<	"11. Bekasi;12. Bogor;13. Cimahi;14. Cirebon;15. Depok\n"
			<<	"16. Sukabumi;17. Tasikmalaya;18. Banjar;19. Magelang;20. Pekalongan\n"
			<<	"21. Purwokerto;22. Salatiga;23. Semarang;24. Surakarta;25. Tegal\n"
			<<	"26. Batu Alur;27. Blitar;28. Kediri;29. Madiun;30. Malang\n"
			<<	"31. Mojokerto;32. Pasuruan;33. Probolinggo;34. Surabaya;35. Pontianak\n"
			<<	"36. Singkawang;37. Banjarbaru;38. Banjarmasin;39. Palangkaraya;40. Balikpanpan\n"
			<<	"41. Bontang;42. Jayapura;43. Makassar;44. Palu;45. Manado\n"
			<<	"46. Kendari;47. Bitung;48. Padang;49. Dumai;50. Yogyakarta\n";
}



void reset(survey sur[], int &n, int sizeofarray){

	//zeroing the value
	for(int i = 0; i < sizeofarray; i++){
		sur[i].kelamin.pria = false;
		sur[i].kelamin.wanita= false;
		sur[i].izin.broker = false;
		sur[i].izin.salesperson = false;
		sur[i].kota = '\0';
		sur[i].umur = '\0';
		sur[i].pendidikan = '\0';
	}

	n = 0;

}


void report(survey sur[], int &n){
	
	string namakota[100] ={"Banda Aceh","Langsa","Meulaboh","Denpasar","Tangerang",
							"Bengkulu","Serang","Gorontalo","Jambi","Bandung",
							"Bekasi","Bogor","Cimahi","Cirebon","Depok",
							"Sukabumi","Tasikmalaya","Banjar","Magelang","Pekalongan",
							"Purwokerto","Salatiga","Semarang","Surakarta","Tegal",
							"Batu Alur","Blitar","Kediri","Madiun","Malang",
							"Mojokerto","Pasuruan","Probolinggo","Surabaya","Pontianak",
							"Singkawang","Banjarbaru","Banjarmasin","Palangkaraya","Balikpanpan",
							"Bontang","Jayapura","Makassar","Palu","Manado",
							"Kendari","Bitung","Padang","Dumai","Yogyakarta"}; 



	for(int i=0;i<n;i++){
	
		//salesperson || broker
		if(sur[i].izin.broker == true && sur[i].izin.salesperson == false){
			cout << setw(12) << "Broker";
		}
		else if(sur[i].izin.broker == false && sur[i].izin.salesperson == true){
			cout << setw(12) << "Salesperson";
		}

		//kota
		cout << setw(12) << namakota[sur[i].kota];


		//umur
		cout << setw(12) << sur[i].umur;

		//sex
		if(sur[i].kelamin.pria == true && sur[i].kelamin.wanita == false){
			cout << setw(12) << "Pria";
		}
		else if(sur[i].kelamin.pria == false && sur[i].kelamin.wanita == true){
			cout << setw(12) << "Wanita";
		}

		//pendidikan
		if(sur[i].pendidikan == 1){
			cout << setw(12) << "Tidak Lulus";
		}
		else if(sur[i].pendidikan == 2){
			cout << setw(12) << "SMU";
		}
		else if(sur[i].pendidikan == 3){
			cout << setw(12) << "S1";
		}
		else if(sur[i].pendidikan == 4){
			cout << setw(12) << "S2";
		}

		cout << endl;

	}



}


int main(){


	//kontrol input dari user
	int input = 0;

	//deklarasi besar array dinamis dengan pointer

	//!!!!!kalo ada masalah, coba mainin di sizeofarray ini  (jadiin 0/1)
	int sizeofarray = 200;		//deklarasi awal(sizeofarray)
	

	int dummy = 0;		//untuk di increment, setiap kali ada data baru 
	int *n;				//deklarasi pointer n
	n = &dummy;			//n adalah referensi alamat memori dari sizeofarray

	//inventory *ptr,inv;
	//ptr = &inv;


	//declare struct
	survey sur[sizeofarray];



//zeroing the value
	for(int i = 0; i < sizeofarray; i++){
		sur[i].kelamin.pria = false;
		sur[i].kelamin.wanita= false;
		sur[i].izin.broker = false;
		sur[i].izin.salesperson = false;
		sur[i].kota = '\0';
		sur[i].umur = '\0';
		sur[i].pendidikan = '\0';
	}




do{

	cout << "\nProgram Kuesioner\n";
	cout << "1. Input baru\n";
	cout << "2. Analisis\n";
	cout << "3. Reset Data\n";
	cout << "4. Lihat Kode Kota\n";
	cout << "5. Exit\n";
	cout << "6. Clear System\n";
	cout << "7. Cetak laporan\n";
	cout << "Input = ";
	cin >> input;

	if(input == 1){
		baru(sur, *n);
	}
	else if(input == 2){
		analisis(sur, *n);
	}
	else if(input == 3){
		reset(sur, *n, sizeofarray);
	}
	else if(input == 4){
		kota();
	}
	else if(input == 6){
		system("cls");
	}
	else if(input == 7){
		report(sur, *n);
	}




}
while(input != 5);
	cout << "Bye!\n";


}
